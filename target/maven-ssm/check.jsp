<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="pages/publicPages/comm.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>验证身份</title>
    <link rel="stylesheet" href="css/resetpwd2.css" />
    <style type="text/css">
        #btnSendCode1{
            width: 150px;
            height: 30px;
            background-color: #ff3000;
            border: 0;
            border-radius: 15px;
            color: #fff;
        }
        #btnSendCode1.on {
            background-color: #eee;
            color: #ccc;
            cursor: not-allowed;
        }
    </style>
</head>
<body>
<div id="app">
    <div class="auth-wrap">
        <div class="header">
            <div class="logo-con w clearfix">
                <a href="stone.jsp" class="logo">Stone</a>
                <div class="logo-title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;验证身份</div>
            </div>
        </div>

        <div class="form-wrap setnew-wrap">
            <div class="form-item">
                <span class="label hide"></span>
                <div class="item-rcol">
                    <div class="item-input-wrap item-input-xl">
                        <input type="text" class="field"  id="phone1" placeholder="请输入手机号" style="height: 52px"
                               required maxlength="11"  onblur="check()">

                        <span class="i-ops"></span>
                    </div>
                    <div class="input-error hide">
                        <span class="i-error iconfont icon-warn"></span>
                    </div>
                </div>
            </div>

            <div class="form-item ac mt20 form-item-validate">
                <input type="text" id="code" required placeholder="验证码" maxlength="6" class="field" style="width: 150px;height:55px;border: 1px solid #BBBBBB;float:left"/>
                <button type="button" id="btnSendCode1"  onClick="sendMessage1()" style="width: 150px;height:52px;border: 1px solid #BBBBBB;float:right">
                    点击获取验证码
                </button>
            </div>
            <div></div>


            <div class="form-item ac mt20">
                <button type="button" id="next" class="btn-primary btn-xl " >下一步</button>
            </div>
        </div>

    </div>
</div>
<jsp:include page="pages/publicPages/footer.jsp"></jsp:include>
</body>
<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
    //验证电话号码手机号码，包含至今所有号段
    /*$("#phone1").blur(function(){*/
    var flag =false;
    function check() {
        var phone = document.getElementById("phone1").value;
        var ab = /^[1][3,4,5,7,8][0-9]{9}$/;
        if(phone==null || phone ==""){
            alert("手机号码不能为空！");
            flag = false;
            return flag;
        }else if (!ab.test(phone)) {
            alert("请输入正确的手机号码！");
            flag = false;
            return flag;
        }else{
            $.getJSON("user/check",{"phone":phone},function (data) {
                if(data){
                    flag = true;
                    return flag;
                }else{
                    alert("手机号码未注册！请输入正确的手机号码！");
                    flag = false;
                    return flag;
                }
            })
        }
    }

    var count = 60; //间隔函数，1秒执行
    var InterValObj1; //timer变量，控制时间
    var curCount1;//当前剩余秒数
    /*第一*/

    function sendMessage1() {
        curCount1 = count;
        var phone = $("#phone1").val();//电话号码

        // 发送验证码
        $.getJSON("user/sendSms",{"phoneNo":phone})
        //设置button效果，开始计时
        $("#btnSendCode1").attr("disabled", "true");
        $("#btnSendCode1").css("pointer-events","none");
        $("#btnSendCode1").html( + curCount1 + "秒再获取");
        InterValObj1 = window.setInterval(SetRemainTime1, 1000); //启动计时器，1秒执行一次
        //向后台发送处理数据

    }

    function SetRemainTime1() {
        if (curCount1 == 0) {
            window.clearInterval(InterValObj1);//停止计时器
            $("#btnSendCode1").removeAttr("disabled");//启用按钮
            $("#btnSendCode1").css("pointer-events","auto");
            $("#btnSendCode1").html("重新发送");
        }
        else {
            curCount1--;
            $("#btnSendCode1").html( + curCount1 + "秒再获取");
        }
    }


    $("#next").click(function(){
        //手机号 验证码
        var phone = $("#phone1").val();
        var code = $("#code").val();
        if(flag){
            if($("#code").val()==null ||$("#code").val() =="" ){
                alert("验证码不能为空");
            }
            $.getJSON("user/forgetpwd",{"phone":phone,"code":code},function(data){
                if(data){
                    window.location.href="changepwd.jsp?phone="+phone+"";
                }else {
                    alert("输入的验证码错误！")
                }
            })
        }
    })
</script>
</html>
