$(function () {
   /* $(window.parent.document).find("#iframe").load(function () {
        var main = $(window.parent.document).find("#iframe");
        var thisheight = $(document).height() + 250;
        main.height(thisheight);
    })*/
    function getUrlParam(key) {
        // 获取参数
        var url = window.location.search;
        // 正则筛选地址栏
        var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
        // 匹配目标参数
        var result = url.substr(1).match(reg);
        //返回参数值
        return result ? decodeURIComponent(result[2]) : null;
    }
    var id = getUrlParam("id");
    getNickName();
    function getNickName(){
        var random = parseInt(Math.random()*10000);
        $.getJSON("/Game/user/queryUserById?random="+random,{"id":id},function (data) {
            var nickname = data.nickName;
            var phone = data.phone;
            var email = data.email;
            if (nickname != "" && nickname != null){
                $("#bNickName").html(nickname)
            } else if (phone != "" && nickname != null){
                $("#bNickName").html(phone)
            }
        })
    }


    $("#modify").click(function(){
        $(".member-info").css("display","none");
        $("#modifyname").css("display","block");
    })
    $("#nameCancle").click(function () {
        $("#modifyname").css("display","none");
        $(".member-info").css("display","block");
    })
    $("#aConfirm").click(function () {
        var nickname = $("#nickname").val();
        $.getJSON("/Game/user/updateUserId",{"id":id,"nickName":nickname})
        $("#modifyname").css("display","none");
        $(".member-info").css("display","block");
        location.reload();
    })

})