<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>增加商品</title>
    <link rel="stylesheet" href="../css/public/head.css">
    <link rel="stylesheet" href="../css/public/footer.css">
    <style type="text/css">

        #blank0{
            height:50px;
            width: 100%;
        }
        .blank1{
            height: 30px;
            width: 100%;
        }
        #addgame{
            margin-left: 650px;
        }
        #errorname,#errortime{
            color: red;
            font-size: 18px;
            font-family: 宋体;

        }
        #tittle span{
            font-family: 楷体;
            font-weight: bolder;
            font-size: 40px;
            margin-left: 750px;
        }
        input[type=text],textarea{
            border: 1px solid #ccc;
            padding: 7px 0px;
            border-radius: 3px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s
        }
        input[type=date]::-webkit-inner-spin-button {
            visibility: hidden;
        }

    </style>
</head>
<body>
<div id="blank0"></div>
<div id="tittle"><span>添加游戏</span></div>
<div id="addgame">
<form id="gameMsg">
    <table>
        <tr>
            <td style="font-family: 楷体;font-weight: bolder;font-size: 20px;">游戏名</td>
            <td><input type="text" id="name" name="name" size="35" height="25" onblur="checkName()"></td>
            <input type="hidden" id="pictureId" name="pictureId" value="" />
        </tr>
        <tr>
            <td style="font-family: 楷体;font-weight: bolder;font-size: 20px;">游戏类型</td>
            <td><input type="text" name="type" size="35"></td>
            <td><div class="blank1"></div></td>
        </tr>
        <tr>
            <td style="font-family: 楷体;font-weight: bolder;font-size: 20px;">游戏价格</td>
            <td><input type="text" name="price" size="10"></td>
            <td><div class="blank1"></div></td>
        </tr>
        <tr>
            <td style="font-family: 楷体;font-weight: bolder;font-size: 20px;">游戏发行商</td>
            <td><input type="text" name="publisher" size="35"></td>
            <td><div class="blank1"></div></td>
        </tr>
        <tr>
            <td style="font-family: 楷体;font-weight: bolder;font-size: 20px;">游戏开发商</td>
            <td><input type="text" name="developers" size="35"></td>
            <td><div class="blank1"></div></td>
        </tr>
        <tr>
            <td style="font-family: 楷体;font-weight: bolder;font-size: 20px;">支持平台</td>
            <td><input type="text" name="platFrom" size="35"></td>
            <td><div class="blank1"></div></td>
        </tr>
        <tr>
            <td style="font-family: 楷体;font-weight: bolder;font-size: 20px;">发行时间</td>
            <td><input type="date" name="issueDate" id="time" size="35"  max="2019-11-15"></td>
            <td><div id="errortime"></div></td>
            <td><div id="blank1"></div></td>
        </tr>
        <tr>
            <td style="font-family: 楷体;font-weight: bolder;font-size: 20px;">评测</td>
            <td><textarea name="evaluation" style="resize: none;width: 100%;height: 150px" size="5000"></textarea></td>
            <td><div class="blank1"></div></td>
        </tr>
        <tr>
            <td style="font-family: 楷体;font-weight: bolder;font-size: 20px;">简介</td>
            <td><textarea name="content" style="resize: none;width: 100%;height: 150px" size="5000"></textarea></td>
            <td><div class="blank1"></div></td>
        </tr>
        <tr>
            <td style="font-family: 楷体;font-weight: bolder;font-size: 20px;">封面图片</td>
            <td><input type="file" id="pic" name="pic" accept="image/*" onchange="setImg('pic')"></td>
            <td><div class="blank1"></div></td>
        </tr>
        <tr>
            <td style="font-family: 楷体;font-weight: bolder;font-size: 20px;">详情图片</td>
            <td><input type="file" id="pic2" name="pic2" accept="image/*" multiple="multiple" value="选择文件" onchange="setImg('pic2')"></td>
            <td><div class="blank1"></div></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <button type="button" id="sub" style="width: 80px;height: 40px;background: #cccccc;font-size: 15px;font-family: 楷体">提交</button>
                <div id="localImag"></div>
            </td>
        </tr>
    </table>
</form>
</div>
<script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script type="text/javascript"src="js/ajaxfileupload.js"></script>
<script type="text/javascript">
    function checkName(){
        var name = $("#name").val();
        //验证游戏名 不能与数据库中有的相同
        $.getJSON("../games/checkname",{"name":name},function (data) {
            if(!data){
                alert("输入的游戏已经存在，请勿重复输入！");
                $("#name").val("");
                //$("#errorname").html("输入的游戏已经存在，请勿重复输入！");
            }
        })
    }

    $("#sub").click(function () {
        if (confirm("确认上传？")){
            submitImg()
        }

    })

    function setImg(id) {
        //补充说明：因为我们给input标签设置multiple属性，因此一次可以上传多个文件
        //获取选择图片的个数
        var dom = document.getElementById(id);
        var files = dom.files;
        var length = files.length;
        //var imgObjPreview = document.getElementById("localImag");
        //3、回显
        //imgObjPreview.innerHTML = "";

       if(length>9){
           $("#sub").attr("disabled", true);
           alert("最多上传9张图片")
       }else{
           $("#sub").removeAttr("disabled");
       }
/*            $.each(files,function(key,value) {
                //每次都只会遍历一个图片数据
                var img = document.createElement("img");
                var imgObjPreview = document.getElementById("localImag");
                var fr = new FileReader();
                fr.onload = function () {
                    img.src = this.result;
                    img.style.width = "50px";
                    img.style.height = "70px";
                    img.style.marginRight = "10px";
                    img.style.marginTop = "10px";
                    img.id = key + "img";
                    imgObjPreview.appendChild(img);
                }
                fr.readAsDataURL(value);//读取文件
            })*/

    }
    function submitImg() {
        var name = $("#name").val();
        $.ajaxFileUpload({
            url: "/Game/ruler/insertTitle",
            type:"post",
            contentType: "multipart/form-data",
            data:{"name":name},
            secureuri:false,
            fileElementId :'pic',
            dataType: "json",
            success: function (data) {
                submitImg2(data);
            },
            error: function () {
                alert("上传失败")
            }
        })
    }
    function submitImg2(picId) {
        var name = $("#name").val();
        $.ajaxFileUpload({
            url: "/Game/ruler/insertDetail",
            type:"json",
            contentType: "multipart/form-data",
            data:{"name":name,"picId":picId},
            secureuri:false,
            fileElementId :'pic2',
            dataType: "json",
            success: function (data) {
                $("#pictureId").val(data);
                $.getJSON("/Game/ruler/addGame",$("#gameMsg").serialize(),function (data) {
                    if (data == "1"){
                        alert("添加成功！")
                        location.href="index.jsp";
                    }

                })
            },
            error: function () {
                alert("上传失败");
            }
        })
    }
</script>
</body>
</html>
