<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>STONE订单提交系统</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description" content=""/>
    <meta name="format-detection" content="telephone=no" />
    <meta name=""/>
    <link rel="stylesheet" href="../css/tasp.css" />
    <link href="../css/orderconfirm.css" rel="stylesheet" />
    <script type="text/javascript" src="../js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
        //获取地址栏参数,可以是中文参数
        function getUrlParam(key) {
            // 获取参数
            var url = window.location.search;
            // 正则筛选地址栏
            var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
            // 匹配目标参数
            var result = url.substr(1).match(reg);
            //返回参数值
            return result ? decodeURIComponent(result[2]) : null;
        }

            function send() {
                var type = $("#typeX").val();
                var userid=$("#userId").val();
                var shopId = $("#sid").val().split(",");
                var price = $("#total").html();
                var addressId = $("input[name='choose']:checked").val();
                var name ="";
                var phone="";
                var address="";
                $.ajaxSettings.async=false;
                $.getJSON("/Game/address/queryAddressById",{"id":addressId},function (data) {
                    name=data.name;
                    phone=data.phone;
                    address=data.province+data.country+data.area+data.detail;
                })
                $.ajaxSettings.async=true;
                var str = "";
                var shopping = document.getElementsByName("yxname");
                var count = document.getElementsByName("count");
                var counts=[];
                for (i=0;i<shopping.length;i++){
                    str+=(shopping[i].value+"&nbsp;&nbsp;x"+count[i].value+"<br>");
                    counts.push(count[i].value);
                }
                 if (addressId==null){
                     alert("请选择地址")
                 }else {
                     var in_name=name;
                     var in_money =price;
                     var ph = phone;
                     $.ajaxSettings.async=false;
                     $.getJSON("/Game/order/addOrder",{"name":name,"shop":str,"phone":phone,"address":address,"totalprice":price,"userid":userid},function (data) {
                         var d=data[0];
                         var c="";
                         var si="";
                         for(var i=0;i<counts.length;i++){
                             si=shopId[i];
                             c=counts[i];
                             var random = parseInt(Math.random()*10000);
                            $.getJSON("/Game/games/orderAndGame?random="+random,{"order":d,"shopid":si,"count":c});
                            if (type == 2){
                            }else {
                                $.getJSON("/Game/user/delCartByUidAndSid?random="+random,{"uid":userid,"sid":shopId[i]});
                            }
                         }

                         window.location.href = "/Game/aliPay/getMsg?in_name=" + in_name
                             + "&&in_money=" + in_money +"&&in_phone=" + ph;
                     })
                     $.ajaxSettings.async=true;
                 }
            }


        //表单校验
         $(function () {
             $("#phone").focus(function () {
                 $("#jyPhone").html("");
             })
             $("#detail").focus(function () {
                 $("#jyXx").html("");
             })
             $("#name").focus(function () {
                 $("#jyName").html("");
             })
             $("#phone").blur(function () {
                 var phone = $(this).val();
                var jy = /^1[35759]\d{9}$/;
                if (jy.test(phone)==false){
                     $("#jyPhone").html("手机号格式错误！");
                    return false;
                }else {
                     $("#jyPhone").html("");
                    return true;
                 }
            })
             $("#name").blur(function () {
                 var name = $(this).val();
                 if (name==""){
                     $("#jyName").html("姓名不能为空！");
                     return false;
                 }else {
                     $("#jyName").html("");
                     return true;
                 }
             })
             $("#detail").blur(function () {
                 var xx = $(this).val();
                 if (xx==""){
                     $("#jyXx").html("详细地址不能为空！");
                     return false;
                 }else {
                     $("#jyXx").html("");
                     return true;
                 }
             })
         })
        //删除地址
        function del(id,obj) {
            if(confirm("确认删除该地址？")){
                $.getJSON("address/del",{"id":id},function (data) {
                    if(data){
                        alert("删除成功！");
                        $(obj).parents("li").remove();
                    }
                })
            }
        }

        //点击修改地址
        function xg() {
            var jylength = document.getElementsByName("choose").length;
            if (jylength>4){
                alert("地址最多存放5条，请删除后增加。");
            }else{
                document.getElementById("xgk").style.display="block";
            }
        }

        function qx() {
            document.getElementById("xgk").style.display="none";

        }


        function qr() {
             //验证表单
            var detail= $("#xx").val();
            var phone= $("#phone").val();
            var name= $("#name").val();
            var jy = /^1[35759]\d{9}$/;
            if (detail==""||detail==" "){
                return false;
            }else if (phone==""||jy.test(phone)==false){
                return false;
            }else if (name==""){
                return false;
            }else{
                var province = $("#cmbProvince option:selected").val();
                var country = $("#cmbCity option:selected").val();
                var area = $("#cmbArea option:selected").val();
                var detail = $("#detail").val();
                var phone = $("#phone").val();
                var name = $("#name").val();
                var userId = $("#userId").val();
                $.getJSON("/Game/address/add",{"province":province,"country":country,"area":area,"detail":detail,"phone":phone,"name":name,"userId":userId},function (data) {
                    if (data){
                        alert("添加成功");
                        window.history.go(0);
                    }
                })
                document.getElementById("xgk").style.display="none";
                return true;
            }
        }
        $(function () {
            //读取购买游戏
            var type = $("#typeX").val();
            var note = "";
            var games="";
            var ids= new Array($("#sid").val());
            var uid=$("#userId").val();
            $.getJSON("/Game/games/queryGameById",{"id":ids},function (data) {
                note+="";
                games+="";
                    $(data).each(function () {
                        var gid=this.id;
                        var count="";
                        if (type == 2){
                            count = 1;
                        }else{
                            $.ajaxSettings.async=false;
                            $.getJSON("/Game/games/queryCount",{"gid":gid,"uid":uid},function (data) {
                                count=data.toString();
                            })
                            $.ajaxSettings.async=true;
                        }
                        note+="<tr style='line-height: 40px'>" +
                            "<td style='font-size: 20px;position:relative;left: 150px'>"+
                            this.name+
                            "</td>"+
                            "<td style='font-size: 15px;position:relative;left: 360px'>¥"+
                            this.price+
                            "</td>"+
                                "<td  style='font-size: 15px;position:relative;left: 500px'>"+count+"<input name='count' style='display: none' value='"+count+"'/>"+"</td>"+
                            "<td style='font-size: 15px;position:relative;left: 640px'>¥"+
                                this.price+
                            "</td></tr>"
                        games+="<input type='text' style='display: none' name='yxname' value='"+this.name+"'/>";
                    })
                    $("#shop").append(note);
                    $("#games").append(games);

            })


            $("#game").append();
            //读取用户地址
            $.getJSON("/Game/address/queryAll",{"userId":$("#userId").val()},function (data) {
                var str="";
                $(data).each(function () {
                    str+="<li><input type='radio' name='choose'";
                    if (this.status==1){
                        str+="checked"
                    }
                    str+=" value='"+this.id+"'>"+
                        "&nbsp"+
                        this.province+
                        this.country+
                        this.area+
                        "-"+
                        this.detail+
                        "&nbsp&nbsp"+
                        this.name+
                        this.phone+
                        "&nbsp<a href='#' onclick='del("+this.id+",this)'>删除</a></li>"
                })
                $("#address-list").append(str);
            })
        })
    </script>
    <style>
        .form-control{
            margin: 20px;
            width: 86px;
            height: 30px;
            font-size: 15px;
        }
        #page{width:auto;}
        #comm-header-inner,#content{width:950px;margin:auto;}
        #logo{padding-top:26px;padding-bottom:12px;}
        #header .wrap-box{margin-top:-67px;}
        #logo .logo{position:relative;overflow:hidden;display:inline-block;width:140px;height:35px;font-size:35px;line-height:35px;color:#f40;}
        #logo .logo .i{position:absolute;width:140px;height:35px;top:0;left:0;background:url(http://a.tbcdn.cn/tbsp/img/header/logo.png);}
    </style>

</head>
<body data-spm="1">
<input type="hidden" id="price" value="<%=request.getParameter("totalprice")%>"/>
<input type="hidden" id="sid" value="<%=request.getParameter("sid")%>"/>
<input type="hidden" id="uid" value="<%=session.getAttribute("uid")%>"/>
<input type="hidden" id="userId" value="<%=session.getAttribute("id")%>"/>
<input type="hidden" id="count" value="<%=request.getParameter("count")%>"/>
<input type="hidden" id="typeX" value="<%=request.getParameter("typeX")%>"/>
<div id="page">
    <div id="content" class="grid-c">
        <div id="address" class="address" style="margin-top: 20px;" data-spm="2">
                <h3>确认收货地址
                    <span class="manage-address">
 </span>
                </h3>
                <ul id="address-list" class="address-list"  style="font-size: 20px">
                    <div id="games">

                    </div>
                </ul>

                <div class="address-bar">
                    <a   onclick="xg()" class="new J_MakePoint" >使用新地址</a>
                </div>
                <div id="xgk" style="width: 555px;height: 233px;border:2px;display: none;background-color:#f2f7ff">
                    <form>
                   <!--修改框-->
                    <table style="line-height: 35px;width:100% ">
                        <tr>
                        <select class="form-control" id="cmbProvince" name="province"></select>
                        <select class="form-control" id="cmbCity" name="country"></select>
                        <select class="form-control" id="cmbArea" name="area"></select>
                        <script type="text/javascript" src="../js/jquery-1.11.1.min.js"></script>
                        <script type="text/javascript" src="../js/address.js"></script>
                        <script type="text/javascript">
                            $(document).ready(function() {
                                addressInit('cmbProvince', 'cmbCity', 'cmbArea');
                            });
                        </script>
                        </tr>
                        <tr>
                            <td style="font-size: 18px;width:20%"><span style="float: right">详细地址：</span></td>
                            <td style="font-size: 18px"><input name="detail" id="detail" type="text"><span id="jyXx" style="color: red"></span></td>

                        </tr>
                        <tr>
                            <td style="font-size: 18px;width:20%"><span style="float: right">手机号：</span></td>
                            <td style="font-size: 18px"><input name="phone" id="phone" type="text"><span style="color: red" id ="jyPhone"></span></td>
                        </tr>
                        <tr>
                            <td style="font-size: 18px;width:20%"><span style="float: right">姓名：</span></td>
                            <td style="font-size: 18px"><input name="name" id="name"  type="text" maxlength="7" ><span style="color: red" id="jyName"></span></td>
                        </tr>
                    </table>
                        <button type="reset" onclick="qx()"  style="width:80px;height:35px;float: right;margin-right: 41px ">取消</button> <button type="button" id="btn" onclick="qr()"  style="width:80px;height:35px;float: right;margin-right: 41px ">确定</button>
                    </form>
                </div>
        </div>
        <form id="J_Form" name="J_Form" action="/auction/order/unity_order_confirm.htm" method="post">
            <input name='_tb_token_' type='hidden' value='IZpONoL2bm'>
            <div>
                <h3 class="dib">确认订单信息</h3>
                <table cellspacing="0" cellpadding="0" class="order-table" id="J_OrderTable" summary="统一下单订单信息区域">
                    <caption style="display: none">统一下单订单信息区域</caption>
                    <thead>
                    <tr>
                        <th class="s-title">店铺宝贝<hr/></th>
                        <th class="s-price">单价(元)<hr/></th>
                        <th class="s-amount">数量<hr/></th>
                        <th class="s-total">小计(元)<hr/></th>
                    </tr>
                    </thead>
                    <tbody data-spm="3" class="J_Shop" data-tbcbid="0" data-outorderid="47285539868"  data-isb2c="false" data-postMode="2" data-sellerid="1704508670" >
                    <tr class="first"><td colspan="5"></td></tr>
                    <tr class="shop blue-line">
                        <td colspan="3">
                            商家：<a href="../stone.jsp"><span>STONE</span></a>
                        </td>
                        <td colspan="2" class="promo">
                            <div>
                                <ul class="scrolling-promo-hint J_ScrollingPromoHint">
                                </ul>
                            </div>
                        </td>
                    </tr>

                    <td id="shop"></td>

                    <tr class="item-service">
                        <td colspan="5" class="servicearea" style="display: none"></td>
                    </tr>
                    <tr class="blue-line" style="height: 2px;"><td colspan="5"></td></tr>
                    <tr class="other other-line">
                        <td colspan="5">
                            <ul class="dib-wrap">
                                <li class="dib extra-info">
                                    <div class="shoparea">
                                        <ul class="dib-wrap">

 <em class="style-normal-bold-black J_ShopPromo_Result"  >0.00</em>
  </span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="shoppointarea"></div>
                                    <div class="farearea">
                                        <ul class="dib-wrap J_farearea">
                                            <li class="dib title">运送方式：</li>
                                            <li class="dib sel" data-point-url="http://log.mmstat.com/jsclick?cache=*&tyxd=wlysfs">
                                            <p>包邮</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="extra-area">
                                        <ul class="dib-wrap">
                                            <li class="dib title">发货时间：</li>
                                            <li class="dib content" >卖家承诺订单在买家付款后，72小时内发货</li>
                                        </ul>
                                    </div>

                                    <div class="servicearea" style="display: none"></div>
                                </li>
                            </ul>
                        </td>
                    </tr>

                    <tr class="shop-total blue-line">
                        <td colspan="5">店铺合计：
                            <span class='price g_price '>
                                <span>&yen;</span><span id="total"><%=request.getParameter("totalprice")%></span>
                            </span>
                            <input type="hidden" name="1704508670:2|creditcard" value="false" />
                            <input type="hidden" id="J_IsLadderGroup" name="isLadderGroup" value="false"/>

                        </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="5">
                            <div class="order-go" data-spm="4">
                                <div class="J_AddressConfirm address-confirm">
                                    <div class="kd-popup pop-back" style="margin-bottom: 40px;">
                                        <a href="#"
                                           class="back J_MakePoint" target="_top"
                                           data-point-url="">返回购物车</a>
                                        <a id="J_Go" class=" btn-go"   onclick="send()"  tabindex="0" title="点击此按钮，提交订单。">提交订单<b class="dpl-button"></b></a>
                                    </div>
                                </div>
                                <div class="msg" style="clear: both;">
                                    <p class="tips naked" style="float:right;padding-right: 0">若价格变动，请在提交订单后联系卖家改价，并查看已买到的宝贝</p>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </form>
    </div>
    <div id="footer"></div>
</div>
<div style="text-align:center;">
</div>
</body>
</html>
