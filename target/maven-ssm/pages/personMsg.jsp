<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="publicPages/comm.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <title>STONE</title>
</head>
<style type="text/css">
    body{
        width: 80%;
        margin: auto;
    }
</style>
<body>
<div style="width:1500px ;margin: auto">
<!--页面顶部-->
<jsp:include page="publicPages/head.jsp"></jsp:include>
<!--页面中部-->
<div id="content" class="main-content clearfix" style="width: 80%;margin-left: 15%">
    <jsp:include page="publicPages/left.jsp"></jsp:include>
    <jsp:include page="publicPages/right.jsp"></jsp:include>
</div>
<!--页面底部-->
<jsp:include page="publicPages/footer.jsp"></jsp:include>
</div>
</body>
</html>
