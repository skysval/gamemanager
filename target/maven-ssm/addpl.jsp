<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="pages/publicPages/comm.jsp"%>
<html>
<head>
    <title>评论</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/addpl/p1.css"/>
    <link rel="stylesheet" href="css/addpl/p2.css"/>
    <link rel="stylesheet" href="css/addpl/p3.css"/>
    <link rel="stylesheet" href="css/addpl/p4.css"/>
    <link rel="stylesheet" type="text/css" href="css/addpl/dialog.css">
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.dialog.js"></script>
    <script type="text/javascript">
        //获取地址栏参数,可以是中文参数
        function getUrlParam(key) {
            // 获取参数
            var url = window.location.search;
            // 正则筛选地址栏
            var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
            // 匹配目标参数
            var result = url.substr(1).match(reg);
            //返回参数值
            return result ? decodeURIComponent(result[2]) : null;
        }


        $(function () {
            // 传值过来的商品id
            //var id = getUrlParam("id");
            var id = 4;
            // 用户id  应该session获取
            var u_id = 1;
            // 评论数量
            var plNum = 0;

            //添加评论
            $("#sendSuccess").click(function () {
                // 获取评论的内容
                var plContext = $("#plcontext").val();
             if(plContext == null || plContext ==""){
                 $(function () {
                     $.sendSuccess('提交评论失败', 3000, function() {
                         console.log('sendSuccess closed');
                     });
                 })
             }else{
                 $.getJSON("pl/add",{"id":id,"u_id":u_id,"context":plContext,"plnum":plNum},function (data123) {
                     if(data123){
                         $(function () {
                             $.sendSuccess('评论成功', 3000, function() {
                                 console.log('sendSuccess closed');
                                 window.location.reload();
                             });
                         })
                     }else{
                         alert("提交失败！")
                         $(function () {
                             $.sendSuccess('提交评论失败', 3000, function() {
                                 console.log('sendSuccess closed');
                             });
                         })
                     }
                 })
             }
            })

            // 获取所有评论
            var strt1="";
            $.getJSON("pl/query",{"id":id},function (data1) {
                if(data1==null || data1 ==""){
                    strt1+="<tr><td class=\"tm-col-master\">"+
                            "<div class=\"tm-rate-content\" style=\"text-align: right;font-size:25px;font-weight:bolder\;font-family: 楷体;\">" +
                            "该游戏目前没有评论</div>"+
                            "<td class=\"col-meta\"></td><td class=\"col-author\"></td></tr>";
                }else{
                    $(data1).each(function () {
                        plNum++;
                        strt1+="<tr><td class=\"tm-col-master\">"+
                            "<div class=\"tm-rate-content\" style=\"font-size:16px;font-weight:bolder\">"+this.context+"</div>" +
                            "<div class=\"tm-m-photos\">";

                        strt1+="<div class=\"tm-rate-date\">"+this.time+"</div></td><td class=\"col-meta\"></td>" +
                            "<td class=\"col-author\"><div class=\"rate-user-info\"><span \"font-size:12px;font-weight:bolder;color: red;\">"+
                            ""+this.user.nickName+"</span></div></td></tr>";
                    })
                }
                $(".rate-grid tbody").append(strt1);
            });

            var str1="";
            var str2="";
            var str3="";
            $.getJSON("games/queryById", {"id":id}, function (data){
                str1+="<a href="+id+"\"searchDetails.jsp?id=\"><img align=\"absmiddle\" src='"+data.picture.picture1+"' style=\"height: 463px\"></a>";
                str2+="<li class=\"ui-form-row\">"+
                    "<label class=\"ui-form-label\">"+
                    "<h3 style='font-size: 32px'>"+data.name+"</h3></label></li>"+
                    " <li class=\"ui-form-row superstar-price\">" +
                    "<label class=\"ui-form-label\">价格</label>" +
                    "<div class=\"ui-form-right\"> <strong>"+data.price+"</strong>" +
                    "<span></span>元</div></li>" +
                    "<li class=\"ui-form-row\"></li>" +
                    "<li class=\"ui-form-row evalate\">" +
                    "<label class=\"ui-form-label\">评价</label>" +
                    "<div class=\"ui-form-right\" ><span>(累计评价" +
                    "<span class=\"superstar-ratetotal\">"+plNum+"</span>)" +
                    "</span></div></li>";
                str3+="<span>累计评价<em class=\"superstar-ratetotal\">("+plNum+")</em></span>"
                $(".db-showpanel").append(str1)
                $(".db-icbu .ui-form-bd").append(str2);
                $(".tv-lb-content").append(str3);
            })
        })
    </script>

</head>
<body>
        <%@include file="pages/publicPages/head.jsp"%>
        <div class="blank0" style="height: 30px"></div>
        <div id="content">
            <div id="mainContent">
                <!-- 五零提供的商品、tab区域 -->
                <div id="detailBox">
                    <div class="db-showpanel">

                    </div>
                    <div class="db-icbu">
                        <ol class="ui-form-bd">

                        </ol>
                        <ol class="scinfo-bd">
                            <li class="ui-form-row">
                                <div class="ui-form-right">
                                    <div class="ui-form-valid">
                                        <div class="ui-msg ui-form-msg">
                                            <p class="ui-msg-con ui-msg-tip">
                                                现在查看的是 您所购买商品的信息
                                                <br>于2019年11月8日下单购买了此商品
                                                <s class="ui-msg-icon"></s>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="banner-totalevolute">
                    <div class="tv-leftbox">
                        <div class="tv-lb-head"></div>
                        <div class="tv-lb-content">

                        </div>
                        <div class="tv-lb-bottom"></div>
                    </div>
                    <div class="tv-rightbox">
                        <div class="tv-rb-cover">
                            <a href="#miaoposition" hidefocus="true" name="miaoposition" id="miaoposition">&nbsp;</a>
                        </div>
                        <div class="tv-rb-bottom"></div>
                    </div>
                </div>

                <!-- 写评价组件 -->
                <div id="review">
                    <div class="rate-compose">
                            <input type="hidden" name="callback" value="">
                            <input type="hidden" name="um_token" value="Tfd1d0a7a20d7d5bbbd4e6230afd598ce">
                            <input type="hidden" name="_tb_token_" value="">
                            <input type="hidden" name="action" value="append_rate_write_action">
                            <input type="hidden" name="event_submit_do_write" value="any">
                            <input type="hidden" name="sellerId" value="3686587643">
                            <div class="append-main"><div class="append-header">
                                <span>其他买家，需要你的建议哦！</span>
                                <div class="append-scrolltip"><s></s></div>
                            </div>
                            <input type="hidden" name="orderIds" value="566489967302453612">
                            <input type="hidden" name="itemId566489967302453612" value="600868170358">
                            <input type="hidden" name="eventId566489967302453612" value="">
                            <input type="hidden" name="parentOrderId" value="566489967302453612">
                            <div class="append-order append-order-last clearfix">
                                <ul class="append-holder">
                                    <li class="ap-prvious">
                                        <div class="ap-timeline"></div>
                                    </li>
                                <li class="ap-this">
                                    <div class="ap-timeline"></div>
                                    <div class="ap-tag ">
                                        <span class="ap-icon"></span>
                                        <div class="ap-title">使用感受 :</div>
                                        <div class="ap-date"></div>
                                    </div>
                                    <div class="ap-content">
                                        <div class="ap-ct-textinput">
                                            <textarea id="plcontext" contenteditable="true" ></textarea>
                                        </div>
                                    </div>
                                </li>
                                </ul>
                            </div>
                            </div>
                         <div class="compose-submit">
        				    <span class="compose-btn">
        					<div class="mt20">
                                <button  id="sendSuccess" type="submit">提交评价</button>
                            </div>
        				    </span>
                        </div>
                    </div>

                    <!--	所有评论-->
                    <div class="banner-totalevolute">
                        <div class="tv-leftbox">
                            <div class="tv-lb-head"></div>
                            <div class="tv-lb-content">

                            </div>
                            <div class="tv-lb-bottom"></div>
                        </div>
                    </div>


                    <div class="rate-grid">
                        <table style="border-top: 1px solid #cccccc;">

                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <%@include file="pages/publicPages/footer.jsp"%>
</body>
</html>
