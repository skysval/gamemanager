<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="pages/publicPages/comm.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>重置密码</title>
    <link rel="stylesheet" href="css/resetpwd2.css" />
    <style type="text/css">
        #btnSendCode1{
            width: 150px;
            height: 30px;
            background-color: #ff3000;
            border: 0;
            border-radius: 15px;
            color: #fff;
        }
        #btnSendCode1.on {
            background-color: #eee;
            color: #ccc;
            cursor: not-allowed;
        }
    </style>
</head>
<body>
<div id="app">
    <div class="auth-wrap">
        <div class="header">
            <div class="logo-con w clearfix">
                <a href="stone.jsp" class="logo">Stone</a>
                <div class="logo-title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;重置密码</div>
            </div>
        </div>

        <div class="form-wrap setnew-wrap">
            <div class="form-item">
                <span class="label hide"></span>
                <div class="item-rcol">
                    <div class="item-input-wrap item-input-xl">
                        <input type="password" class="field"  id="pwd1" placeholder="请输入密码" style="height:52px;" maxlength="10" required  onblur="check()">

                        <span class="i-ops"></span>
                    </div>
                    <div class="input-error hide">
                        <span class="i-error iconfont icon-warn"></span>
                    </div>
                </div>
                <div>
                    <span >&nbsp;&nbsp;需包含数字和大小写字母中至少两种字符的6-10位字符</span>
                </div>
            </div>

            <div class="form-item ac mt20 form-item-validate">
                <input type="password" class="field"  id="pwd2" placeholder="请再次输入密码" style="height:52px; border:1px solid #BBBBBB;width:400px"  required  >

            </div>
            <div><span id="newpwd"></span></div>
            <div class="form-item ac mt20">
                <button type="button" id="change" class="btn-primary btn-xl ">修改密码</button>
            </div>
        </div>

    </div>
</div>
<jsp:include page="pages/publicPages/footer.jsp"></jsp:include>
</body>
<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
    function check(){
        var password = /^[a-zA-Z0-9]{6,10}$/;
        var pwd1 = $("#pwd1").val();
        if(!(password.test(pwd1))){
            alert("输入的密码需包含数字和大小写字母中至少两种字符的6-10位字符")
        }
    }

    $("#change").click(function(){
        // 获取传送过来的参数电话;
        var phoneNo = getUrlParam("phone");
        var pwd1 = $("#pwd1").val();
        var pwd2 = $("#pwd2").val();
        if(pwd1 != pwd2){
            $("#newpwd").val("俩次输入的密码不相同")
        }else{
            $("#change").click(function () {
                $.getJSON("user/updatepwd",{"pwd":pwd2,"phone":phoneNo},function (data) {
                    if(data){
                        alert("修改密码成功");
                        window.location.href="login.jsp";
                    }else{
                        alert("重置密码失败！")
                    }
                })
            })
        }
    })
    //获取地址栏参数,可以是中文参数
    function getUrlParam(key) {
        // 获取参数
        var url = window.location.search;
        // 正则筛选地址栏
        var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
        // 匹配目标参数
        var result = url.substr(1).match(reg);
        //返回参数值
        return result ? decodeURIComponent(result[2]) : null;
    }
</script>
</html>