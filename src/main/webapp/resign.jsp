<%--
  Created by IntelliJ IDEA.
  User: LENOVO
  Date: 2019/11/5
  Time: 16:18
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>注册页</title>
    <link rel="stylesheet" href="css/base.css" />
    <link rel="stylesheet" href="css/public/register.css" />
    <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
    <script type="text/javascript">
        var phoneReg = /(^1[3|4|5|7|8]\d{9}$)|(^09\d{8}$)/;//手机号正则
        var count = 60; //间隔函数，1秒执行
        var InterValObj1; //timer变量，控制时间
        var curCount1;//当前剩余秒数
        /*第一*/
        function sendMessage1() {
            curCount1 = count;
            var phone = $.trim($('#phone1').val());
            if (!phoneReg.test(phone)) {
                alert(" 请输入有效的手机号码");
                return false;
            }
            $.getJSON("user/checkPhone",{"phoneNo":phone},function (data) {
                    data = parseInt(data);
                    if (data == 1){
                        alert("该手机号已注册")
                    }else{
                        //向后台发送处理数据
                        $.getJSON("user/sendSms",{"phoneNo":phone})
                        //设置button效果，开始计时
                        $("#btnSendCode1").attr("disabled", "true");
                        $("#btnSendCode1").css("pointer-events","none");
                        $("#btnSendCode1").html( + curCount1 + "秒再获取");
                        InterValObj1 = window.setInterval(SetRemainTime1, 1000); //启动计时器，1秒执行一次

                    }
            })

        }
        function SetRemainTime1() {
            if (curCount1 == 0) {
                window.clearInterval(InterValObj1);//停止计时器
                $("#btnSendCode1").removeAttr("disabled");//启用按钮
                $("#btnSendCode1").css("pointer-events","auto");
                $("#btnSendCode1").html("重新发送");
            }
            else {
                curCount1--;
                $("#btnSendCode1").html( + curCount1 + "秒再获取");
            }
        }
        $(function () {
            $(".tableBtn").click(function () {
                var pwd1 = $("#pwd1 input[type=password]").val();
                var pwd2 = $("#pwd2 input[type=password]").val();
                var phone = $("#phone1").val();
                var code = $("#code1").val();
                var flag = 0;
                if(pwd1 == pwd2){
                    $.getJSON("user/resgin",{"phoneNo":phone,"code":code,"pwd":pwd2},function (data) {
                        flag = parseInt(data);
                        if(flag != 0){
                            alert("注册成功！")
                            window.location.href="stone.jsp";
                        }
                    })
                }else{
                    alert("请输入相同的密码");
                }
            })
        })


        /*/!*提交*!/
        function binding(){
            alert(1)
        }*/
    </script>
</head>

<body>
<!--头部公共导航-->
<header id="headNav">
    <div class="innerNav clear">
        <a class="fl" href="#">
            <img src="img/log.png" alt="stone"/>
        </a>
        <div class="headFont fr">
            <span>您好，欢迎光临stone！ </span>
        </div>
    </div>
</header>
<!--注册页主体-->
<section id="secTab">
    <div class="innerTob">
        <h2>stone注册</h2>
        <form action="#" method="post">
            <div class="tableItem">
                <input id="phone1" autocomplete="off" type="text" required placeholder="手机号" maxlength="30"/>
            </div>
            <div class="clear">
                <div class="tableItem onlyItem fl">
                    <input id="code1" type="text" autocomplete="off" required placeholder="验证码" maxlength="6"/>
                </div>
                <%--<input id="btnSendCode1" type="button" class="tableText fr" value="获取验证码" onClick="sendMessage1()" />表单验证--%>
                <a id="btnSendCode1" type="button" class="tableText fr" href="#" onClick="sendMessage1()">获取验证码</a>
            </div>
            <div class="tableItem" id="pwd1">
                <input type="password" required placeholder="设置密码" maxlength="16"/>
            </div>
            <div class="tableItem" id="pwd2">
                <input type="password" required placeholder="确认密码" maxlength="16"/>
            </div>
            <p class="clickRegist">点击注册，表示您同意注册<a href="#">《服务协议》 </a></p>
            <button type="button" class="tableBtn">同意协议并注册</button>
        </form>
    </div>
</section>
<!--尾部-->
<footer id="footerNav">
    <p>
        <a href="#">沪ICP备13044278号</a>|&nbsp;&nbsp;合字B1.B2-20130004&nbsp;&nbsp;|<a href="#">营业执照</a>
    </p>
    <p>Copyright © Stone Entertainment Studio 2007-2016，All Rights Reserved</p>
</footer>
</body>
</html>
