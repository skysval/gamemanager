$(function(){
	$(".btn_minus").click(function(){
		var index = $(".btn_minus").index(this)+1
		if($("#num"+index).val()>1){		
			var price = parseFloat($("#evenPrice"+index).html());
			$("#num"+index).val(parseInt($("#num"+index).val())-1);
			$("#totalPrice"+index).val((price*$("#num"+index).val()).toFixed(2));
		}
	})
	$(".btn_plus").click(function(){
		var index = $(".btn_plus").index(this)+1
		var price = parseFloat($("#evenPrice"+index).html());
			$("#num"+index).val(parseInt($("#num"+index).val())+1);
			$("#totalPrice"+index).val((price*$("#num"+index).val()).toFixed(2));
	})
	$(".inputNum").blur(function(){
		var index = $(".inputNum").index(this)+1
		var price = parseFloat($("#evenPrice"+index).html());
		$("#totalPrice"+index).val((price*$("#num"+index).val()).toFixed(2))
	})
	
})
