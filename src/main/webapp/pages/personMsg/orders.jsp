<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/javascript" src="../../js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="../../js/personMsg/orders.js"></script>
<script src="https://cdn.bootcss.com/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../css/PersonMsg/orders.css">
<link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
<style type="text/css">
    .container{
        margin-top: 30px;
    }
    a:hover{
        text-decoration: none;
    }
</style>
<div class="ordercontainer">
    <h3>
        <span>我的订单</span>
    </h3>
    <div class="wtitle">
        <div id='cover'>
        </div>
        <input type="hidden" value="all" id="dingzhiType" name="dingzhiType"/>
        <div id="myTab" class="wtitle_l">
            <a href="javascript:void(0)" style="text-decoration: none" class="track wtitle_lNa" name="my_Index_id_allOrder">全部订单<span></span></a>
            <a href="javascript:void(0)" style="text-decoration: none"  class="track " name="my_Index_id_workingOrder">待付款<span></span></a>
            <a href="javascript:void(0)" style="text-decoration: none"  class="track" name="my_Index_id_finishOrder">待发货<span></span></a>
            <a href="javascript:void(0)" style="text-decoration: none"  class="track" name="my_Index_id_noCommentOrder">待收货<span></span></a>
            <a href="javascript:void(0)" style="text-decoration: none"  class="track" name="my_Index_id_nouseOrder">待评价<span></span></a>
            <a href="javascript:void(0)" style="text-decoration: none"  class="track" name="my_Index_id_noCommentOrder">已完成<span></span></a>
        </div>
    </div>
    <div class="myshop">
        <table id="shoptable" class="shoptable">
            <tr>
                <td colspan="3">
                    商品
                </td>
                <td>单价</td>
                <td>数量</td>
                <td>实付款</td>
                <td>订单状态</td>
            </tr>
            <tr class="blank20"></tr>
        </table>
        <div class="row text-center">
            <nav aria-label="Page navigation">
            </nav>
        </div>
    </div>
</div>
