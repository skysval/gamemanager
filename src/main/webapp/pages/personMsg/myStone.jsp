<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" type="text/css" href="../../css/PersonMsg/myStone.css"/>
<div class="my_cont my_cont12">
    <div class="my_pic">
        <div>
            <div>
                <a href="javascript:void(0)" class="track" name="my_boot_userinfo_updateavatar">
                    <img src="http://user.vanclimg.com/myavatar/vancl300.jpg" width="90" height="90" /></a><span class="head_pic"><a
                    href="/User/UserInfo" class="track" name="my_boot_userinfo_updateavatar">修改头像</a></span></div>
        </div>
        <a href="javascript:void(0)" class="track" name="my_boot_useinfo_edituserinfo">编辑个人资料</a>
    </div>
    <div class="my_txt my_txt12 ">
        <!--infor-->
        <div class="nickname-panel nickname-panel12">
            <div class="nickname-info member-info" style="display: block">
                <a id="anickname">您好！<em id="bNickName"></em></a>
                <a id="modify" href="javascript:void(0);"class="track" name="my_boot_useinfo_Modify">(修改)</a>
            </div>
            <%--<div class="nickname-modify" style="display: none" >
                <input type="text" maxlength="12" name="nickName"/>&nbsp;
                <button id="sub" style="height: 25px">提交</button>&nbsp;
                <button id="back" style="height: 25px">取消</button>
            </div>--%>
            <div class="nickname-info nicknamecanlce" id="modifyname">
                <input type="text" id="nickname" name="nickName" autocomplete="off" value="" maxlength="16"/>
                <button id="aConfirm" class="track" name="my_boot_useinfo_OK"></button>
                <a href="javascript:void(0)" id="nameCancle" class="track" name="my_boot_useinfo_cancel">取消</a>
                <span class="error-tip"><span id="tip">2~16位中英文字符</span></span>
            </div>
            <span class="blank0"></span>
            <div class="txt2 txt212">
                <span class="blank5"></span>
                <ul class="orderStatusul12">
                    <li id="liOrderRelative" class="orderStatusli1201"></li>
                    <li class="orderStatusli1202">账户余额：<a href="javascript:void(0)" target="_blank" class="track underline red12"
                                                          name="my_boot_useinfo_userbalance">￥0</a></li>
                    <li class="orderStatusli1202 orderStatusli1203 ">会员积分：<a href="javascript:void(0)" target="_blank" class="track underline grey12"
                                                                             name="my_boot_useinfo_userpoint">0分</a></li>
                </ul>
                <span class="blank0"></span>
            </div>
            <span class="blank10"></span>
        </div>
        <span class="blank0"></span>
    </div>
</div>
<script type="text/javascript" src="../../js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="../../js/personMsg/myStone.js"></script>