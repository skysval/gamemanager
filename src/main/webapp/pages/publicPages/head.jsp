<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" type="text/css" href="/Game/css/public/head.css"/>
<div id="Head" class="vanclHead" style="margin: auto">
    <div id="headerTopArea" class="headerTopAreaBox">
        <input type="hidden" id="userId" value="<%=session.getAttribute("id")%>">
        <div class="headerTopArea">
            <div class="headerTop">
                <div class="headerTopRight" style="width: 126px;">
                    <div class="active notice" id="vanclCustomer">
                        <a class="mapDropTitle" href="#" target="_blank">网站公告</a>
                    </div>
                    <div class="payattention"><em></em>
                        <a href="#" class="vweixinbox"><span class="vweixin"
                                                             style="background: url(../../img/public/weixin.jpg) no-repeat left 3px;"></span><b
                                class="vweixinTip"></b></a>
                        <a href="#" class="track vanclweibo" name="hp-hp-head-weibo-v:n" target="_blank"
                           style="background: url(../../img/public/weibo.jpg) no-repeat left 3px;"></a>
                    </div>
                </div>
                <div class="headerTopLeft">
                    <div id="welcome" class="top loginArea"> 您好,<span class="top" id="type1" style="color: red"></span>
                        &nbsp欢迎光临Stone！<span id="type0"><a href="/Game/login.jsp" name="hp-hp-head-signin-v:n"
                                                           class="top track">登录</a>&nbsp;|&nbsp;<a
                                href="javascript:location.href='/Game/resign.jsp'" name="hp-hp-head-register-v:n"
                                class="track">注册</a></span><a href="/Game/pages/personMsg.jsp">个人中心</a></div>
                </div>
                <span class="blank0"></span></div>
        </div>
    </div>
    <div id="logoArea" class="vanclLogoSearch">
        <div class="vanclSearch fr">
            <div class="searchTab">
                <div class="search fl">
                    <input type="text" class="searchText ac_input textBox Enter fl" name="k" id="skey" value=""
                           placeholder="刺客信条"/>
                    <input type="button" class="searchBtn sousuoBtn btn" id="btnHeaderSearch" onfocus="this.blur()"/>
                </div>
                <div class="buycar fr active" id="shoppingCarNone">
                    <p>
                        <a id="shoppingcar_link" rel="nofollow" href="/Game/pages/personMsg.jsp?pages=3"
                           name="hp-hp-head-cart-v:n:t"
                           class="shopborder track cartab">购物车<%--(<span car_product_total="shoppingCar_product_totalcount">0</span>)--%></a><s></s>
                    </p>
                    <div class="bottomlines"></div>
                    <div class="BuycarTab shopDropList"></div>
                </div>
            </div>
            <div class="hotWord">
                <p> 热门搜索：
                    <a name="hp-hp-classhotsearch-1_1-v:n" class="track" href="/Game/search.jsp?k=刺客信条"
                       target="_blank">刺客信条</a>
                    <a name="hp-hp-classhotsearch-1_2-v:n" class="track" href="/Game/search.jsp?k=NBA 2K20"
                       target="_blank">NBA 2K20</a>
                    <a name="hp-hp-classhotsearch-1_4-v:n" class="track" href="/Game/search.jsp?k=幽灵行动"
                       target="_blank">幽灵行动</a>
                    <a name="hp-hp-classhotsearch-1_5-v:n" class="track" href="/Game/search.jsp?k=堡垒之夜"
                       target="_blank">堡垒之夜</a>
                </p>
            </div>
        </div>
    </div>
    <div class="navlist clear" id="mainNavBox" style="z-index:300!important;">
        <ul>
            <li class="vancllogo_Con" style="text-align: left;">
                <a href=""></a>
            </li>
            <li>
                <a href="/Game/stone.jsp" class="track">首页</a><span class="NavLine"></span></li>
            <li>
                <a href="/Game/searchkinds.jsp?key=RPG" class="track" name="hp-hp-head-nav_1-v:n"
                   target="_blank">RPG</a><span class="NavLine"></span>
            </li>
            <li>
                <a href="/Game/searchkinds.jsp?key=体育运动" class="track" name="hp-hp-head-nav_4-v:n"
                   target="_blank">体育运动</a><span class="NavLine"></span>
            </li>
            <li>
                <a href="/Game/searchkinds.jsp?key=冒险" class="track" name="hp-hp-head-nav_5-v:n"
                   target="_blank">冒险</a><span class="NavLine"></span>
            </li>
            <li>
                <a href="/Game/searchkinds.jsp?key=动作角色" class="track" name="hp-hp-head-nav_2-v:n"
                   target="_blank">动作角色</a><span class="NavLine"></span>
            </li>
            <li>
                <a href="/Game/searchkinds.jsp?key=竞速" class="track" name="hp-hp-head-nav_6-v:n"
                   target="_blank">竞速</a><span class="NavLine"></span>
            </li>
            <li>
                <a href="/Game/searchkinds.jsp?key=射击" class="track" name="hp-hp-head-nav_7-v:n"
                   target="_blank">射击</a><span class="NavLine"></span>
            </li>
            <li>
                <a href="/Game/searchkinds.jsp?key=角色扮演" class="track" name="hp-hp-head-nav_8-v:n"
                   target="_blank">角色扮演</a><span class="NavLine"></span>
            </li>
            <li>
                <a href="/Game/searchkinds.jsp?key=赛车" class="track" name="hp-hp-head-nav_10-1-v:n"
                   target="_blank">赛车</a><span class="NavLine"></span>
            </li>
        </ul>
    </div>
</div>
<script type="text/javascript" src="/Game/js/jquery-1.12.4.js"></script>
<script type="text/javascript">
    var userId = $("#userId").val()
    if (userId != null && userId != "") {
        $.getJSON("/Game/user/queryUserById", {"id": userId}, function (data) {
            var name = "";
            if (data.nickName != "" && data.nickName != null) {
                name = data.nickName;
            } else if (data.phone != "" && data.phone != null) {
                name = data.phone;
            }
            $("#type0").css("display", "none");
            $("#type1").html(name);
        })
    } else {
        $("#type0").removeAttr("style");
    }
    $(function () {
        $("input[type=button]").click(function () {
            var name = $("#skey").val();
            if (name == "" || name == null) {
                window.location.href = "searchAll.jsp";
            } else {
                window.location.href = "search.jsp?k=" + name + "";
            }
        })
    })

</script>