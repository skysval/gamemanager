<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="pages/publicPages/comm.jsp"%>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=uft-8" />
    <title>Stone</title>
    <link href="css/details_2.css" type="text/css" rel="stylesheet" />
    <link href="css/css1.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="../../js/details.js"></script>

    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
        $(function(){
            var id = getUrlParam("id");
            $(function () {
                var str1="";
                var str2="";
                var str3="";
                var str4="";
                var str5="";
                $.getJSON("games/queryById", {"id":id}, function (data) {
                        str1+="<h2 title='"+data.name+"' style=\"color: black;font-size:40px;font-weight: bolder;margin-left:500px;\">"+data.name+"</h2>";
                    str2+="<table style='line-height: 48px'><tr><td style=\" width: 200px; color: black;font-size: 30px;font-family: 黑体\">游戏类型：</td>" +
                        "<td style=\" color: black;font-size: 25px;font-family: 楷体\">"+data.type+"</td></tr>" +
                        "<tr><td style=\"color: black;font-size: 30px;font-family: 黑体\">游戏生产商：</td>" +
                        "<td style=\"color: black;font-size: 25px;font-family: 楷体\">"+data.publisher+"</td></tr>"+
                        "<tr><td style=\"color: black;font-size: 30px;font-family: 黑体\">游戏发行商：</td>" +
                        "<td style=\"color: black;font-size: 25px;font-family: 楷体\">"+data.developers+"</td></tr>"+
                        "<tr><td style=\"color: black;font-size: 30px;font-family: 黑体\">游戏平台：</td>" +
                        "<td style=\"color: black;font-size: 25px;font-family: 楷体\">"+data.platfrom+"</td></tr>"+
                        "<tr><td style=\"color: black;font-size: 30px;font-family: 黑体\">发行时间：</td>" +
                        "<td style=\"color: black;font-size: 25px;font-family: 楷体\">"+data.issueDate+"</td></tr>"+
                        "<tr style='line-height: 52px'><td><h3 style=\"color: black;font-size: 30px;font-family: 黑体\">售价：</td>" +
                        "<td><span id='price' style=\"color: red;font-size:30px;font:bolder;\">"+data.price+"</span>元</h3></tr></table>";

                        str4+=" <img  src='"+data.picture.picture1+"' title='"+data.name+"' style=\"width:340px;height:500px\"/>";
                        str3+="<p style=\"font-size:15px; \"><span style=\"color: black;font-size: 30px;font-weight:bolder;\">" +
                            "游戏简介:</span>"+data.content+"</p>";
                        str5+= "<img src='"+data.picture.picture2+"' style=\"width: 1920px;height: 1080px;\"/>"+
                            "<img src='"+data.picture.picture3+"' style=\"width: 1920px;height: 1080px;\"/>"+
                            "<img src='"+data.picture.picture4+"' style=\"width: 1920px;height: 1080px;\"/>"+
                            "<img src='"+data.picture.picture5+"' style=\"width: 1920px;height: 1080px;\"/>"+
                            "<img src='"+data.picture.picture6+"' style=\"width: 1920px;height: 1080px;\"/>"+
                            "<img src='"+data.picture.picture7+"' style=\"width: 1920px;height: 1080px;\"/>"+
                            "<img src='"+data.picture.picture8+"' style=\"width: 1920px;height: 1080px;\"/>"+
                            "<img src='"+data.picture.picture9+"' style=\"width: 1920px;height: 1080px;\"/>";


                        $("#productTitle").append(str1);
                        $(".selColorArea").append(str2);
                        $(".content").append(str3);
                        $(".danpinColCenter").append(str4);
                        $(".DetailPicture ").append(str5);
                    });


                // 查找评论
                var strt1="";
                $.getJSON("pl/query",{"id":id},function (data1) {
                    if(data1==null || data1 ==""){
                        strt1+="<div class=\"pinglunTab pinglunT\">"+
                            "<div class=\"pinglunTabLef fl\" style=\"border-top: 0px dotted #b4b4b4;\">"+
                            "<div class=\"pinlunCon fl\">"+
                            "<p>该游戏暂没有评论</p>"+
                            "</div></div><span class=\"blank10\"></span></div>";
                    }else{
                        $(data1).each(function () {
                            strt1+="<div class=\"pinglunTab pinglunT\">"+
                                "<div class=\"pinglunTabLef fl\" style=\"border-top: 0px dotted #b4b4b4;\">"+
                                "<div class=\"pinlunCon fl\">"+
                                "<p>"+this.context+"</p>"+
                                "<span class=\"pinlunTime\">"+this.time+"</span></div></div>"+
                                " <div class=\"pinglunTabRig fr\" style=\"border-top: 0px dotted #b4b4b4;\">"+
                                "<span style='font-size:18px;'>"+this.user.nickName+"</span>"+
                                "<span class=\"blank10\"></span></div></div>";
                        })
                    }
                    $(".NewCommentDetail").append(strt1);
                })
            })
        })
        //获取地址栏参数,可以是中文参数
        function getUrlParam(key) {
            // 获取参数
            var url = window.location.search;
            // 正则筛选地址栏
            var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
            // 匹配目标参数
            var result = url.substr(1).match(reg);
            //返回参数值
            return result ? decodeURIComponent(result[2]) : null;
        }
        function pay() {
            var id = getUrlParam("id");
            if ($("#uid").val()!="null"){
                var price = [parseInt($("#price").html())];
                var count = [1];
                var sid = [id];
                $("#tprice").val(price);
                $("#sid").val(sid);
                $("#count").val(count);
                $("#typeX").val(2);
                document.getElementById('subOneOrder').submit();
            }else {
                alert("请先登录！");
                location.href="/Game/login.jsp";
            }
        }
        function addShoppingCart() {
            var uid = $("#uid").val();
            var sid = getUrlParam("id");
            if (uid != "null"){
                $.getJSON("/Game/user/addShoppingCart",{"uid":uid,"sid":sid,"count":1},function () {
                    alert("已加入购物车");
                })
            }else {
                alert("请先登录！");
                location.href="/Game/login.jsp";
            }

        }
    </script>

</head>

<body id="main" class="cols_manage full">
<%@include file="pages/publicPages/head.jsp"%>
    <div class="danpinBox">
        <span class="blank10"></span>
        <div class="breadNav">
            <div class="danpinArea">
                <div id="ProductTitleShow" class="danpinTitleTab newclear">
                    <div id="productTitle">
                    </div>
                </div>
                <span class="blank30"></span>
                <form action="/Game/pages/note.jsp" id="subOneOrder" name="subOneOrder" style="display: block" method="post">
                    <input type="text" value="" name="totalprice" id="tprice"/>
                    <input type="text" value="" name="sid" id="sid">
                    <input type="text" value="" name="count" id="count">
                    <input type="text" value="2" name="typeX" id="typeX">
                </form>
                <input type="hidden" value="<%=session.getAttribute("id")%>" id="uid">
                <div class="blank0"></div>
                <div class="danpin_colLef">
                    <div class="danpinColCenter">

                        <div id="winSelector" style="left: 0px; top: 0px; display: none;"></div>
                    </div>
                </div>
            <div id="danpinRight" class="danpinRight" style="top: 0px">



                <div class="danpinFixedRightContent">

                    <div class="selectArea">
                        <div class="selColorArea">
                            <span class="blank10"></span>



                            <span class="blank20"></span>
                        </div>
                        <div class="shoppingNews">
                            <button id="nowbuy" name="item-item-select-shopping"
                                    class="btnnowbuy track"  onclick="pay()"><a href="javascript:void(0)">立即购买</a></button>
                            <button id="addToShoppingCar" name="item-item-select-shopping"
                                    onclick="addShoppingCart()" class="btnaddtocart track"></button>
                        </div>
                        <div class="blank0"> </div>
                    </div>
                    <div id="promotion"></div>
                    <div class="blank15">
                    </div>
                </div>

            </div>
        </div>


        <span class="blank20"></span>

        <div class="sideBarSettabArea">

            <div class="RsetTabArea">
                <div id="product_set"> </div>

                <div id="floatposition"></div>
                <span class="blank8"></span>

                <div class="RsetTabCon">
                    <span class="blank15"></span>
                    <div class="area1">
                        <div style="position: relative; top: 0px; right: 0px; z-index: 0">
                            <span class="blank20"></span>
                            <div class="content"></div>
                            <h3 style="font-size: 30px">详情图片：</h3>
                            <span class="blank8"></span>
                            <h3 style="font-weight: normal;"><span>注：</span>
                                <span name="item-item-info-tip">商品实际颜色以静物图为准</span>
                            </h3>

                            <span class="blank5"></span>

                            <div class="imgCon" id="relatedshow">
                                <div class="DetailPicture">

                                </div>
                                <div class="DetailPicBox"> </div>
                            </div>
                        </div>
                    </div>
                    <span class="blank20"></span>
                </div>
            </div>



            <div class="ptPinglunRig " style="width: 980px; padding: 0px;">
                <div class="NewComment">
                    <h2 class="hotTitle" style="position: static; top: 0px; left: 0px; height: 40px;
			        line-height: 40px; padding: 0px 0px 0px 25px; border-bottom: 1px solid #b4b4b4;">
                    <input id="AllPingLun" type="radio" style="margin-left: 5px;" name="ChoosePingLun" checked="checked" value="0" plcount="9" />
                                <label for="AllPingLun" style="margin-right: 5px;">全部评论</label>
                        <span  style="width: 80px;height: 33px; background-color: #d46a6a; color: #fff; display: block; vertical-align: middle;
			                float: right; line-height: 33px;line-height: 33px;">用户名</span>



                    </h2>

                    <div class="pinglunContent" style="margin-left: 34px;">
                        <div class="NewCommentDetail">


                </div>
            </div>
                </div>
            </div>

            <div style="height: 20px;border: 0px ;"></div>
<%@include file="pages/publicPages/footer.jsp"%>
</body>
</html>
