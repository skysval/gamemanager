<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="pages/publicPages/comm.jsp"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title>stone</title>
    <link href='css/css.css' type="text/css" rel="stylesheet" charset="utf-8" />
    <link href='css/css1.css' type="text/css" rel="stylesheet" charset="utf-8" />
    <link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdn.bootcss.com/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
    <script type="text/javascript">
        //获取地址栏参数,可以是中文参数
        function getUrlParam(key) {
            // 获取参数
            var url = window.location.search;
            // 正则筛选地址栏
            var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
            // 匹配目标参数
            var result = url.substr(1).match(reg);
            //返回参数值
            return result ? decodeURIComponent(result[2]) : null;
        }

        var pn = 1;
        var ps = 10;
        var name = getUrlParam("k");
        $(function(){
            query(pn, ps);
        });

        function query(pn, ps) {
            $.getJSON("games/show1",{"pn":pn, "ps":ps,"gameName":name},function (data) {
                var obj = eval(data);
                $("#vanclproducts").empty();
                $(".tsmsg").empty();
                var str = "<ul>";
                var str2="";
                var num=0;
                if(data=="" || data==null){
                    str+="<p>没有找到<span style=\"font-weight:bolder;\">"+name+"</span>相关的内容</p>"
                    str2+="找到和<span>"+name+"</span>相关商品<span>"+num+"</span>款";
                    $("#vanclproducts").append(str);
                    $(".tsmsg").append(str2);
                }else {
                    $(obj.list).each(function () {
                        /* console.log(data)*/
                        num += 1;
                        str += "<li class=\"scListArea borCdbd7d6 productwrapper border\">" +
                            "<div  class=\"pic\">" +
                            "<a href=\"searchDetails.jsp?id=" + this.id + "\" class=\"track\" title='" + this.name + "' target=\"_blank\">" +
                            "<img class=\"productPhoto\" src='" + this.picture.picture1 + "' alt='" + this.name + " 'width=\"230\" height=\"230\" /></a>" +
                            "<div class=\"vancllist_logo\"></div></div>" +
                            "<p><a href=\"searchDetails.jsp?id=" + this.id + "\" class=\"track\" title='" + this.name + "' target=\"_blank\"></a>" + this.name + "</p>" +
                            "<div class=\"Mpricediv0124\">" +
                            "<span class=\"Sprice\">售价￥" + this.price + "</span></div></li>";
                    })
                }
                str2+="找到<span>"+name+"</span>相关商品<span>"+num+"</span>款";
                str += "</ul>";
                $("#vanclproducts").append(str);
                $(".tsmsg").append(str2);

                var page = "";
                page += "<nav aria-label=\"Page navigation\">" +
                    "<ul class=\"pagination\">" +
                    "<li><a href=\"javascript:void(0)\" onclick='query(1,"+ps+")'>首页</a></li>";

                if(obj.hasPreviousPage){
                    page += "<li>" +
                        "<a href=\"javascript:void(0)\" aria-label=\"Previous\" onclick=\"query("+(obj.pageNum-1)+","+ps+")\">" +
                        "<span aria-hidden=\"true\">&laquo;</span>" +
                        "</a>" +
                        "</li>";
                }
                $(obj.navigatepageNums).each(function () {
                    if(obj.pageNum==this){
                        page += "<li class='active'><a href=\"javascript:void(0)\" onclick='query("+this+","+ps+")'>"+this+"</a></li>";
                    }else{
                        page += "<li><a href=\"javascript:void(0)\" onclick='query("+this+","+ps+")'>"+this+"</a></li>";
                    }

                })

                if(obj.hasNextPage){
                    page += "<li>\n" +
                        "<a href=\"javascript:void(0)\" aria-label=\"Next\" onclick='query("+(obj.pageNum+1)+","+ps+")'>" +
                        "<span aria-hidden=\"true\">&raquo;</span>" +
                        "</a>" +
                        "</li>";
                }
                page += "<li><a href=\"javascript:void(0)\" onclick='query("+obj.pages+","+ps+")'>未页</a></li>" +
                    "</ul>" +
                    "</nav>";
                $("nav").empty();
                $("nav").append(page);

            });

            //默认按钮点击时
            $(".moren a").click(function () {
                $("#vanclproducts").empty();
                $(".tsmsg").empty();
                $("nav").empty();
                $.getJSON("games/show1",{"pn":pn, "ps":ps,"gameName":name},function (data) {
                    var obj = eval(data);
                    var str = "<ul>";
                    var str2="";
                    var num=0;
                    if(data=="" || data==null){
                        str+="<p>没有找到<span style=\"font-weight:bolder;\">"+name+"</span>相关的内容</p>"
                        str2+="找到和<span>"+name+"</span>相关商品<span>"+num+"</span>款";
                        $("#vanclproducts").append(str);
                        $(".tsmsg").append(str2);
                    }else {
                        $(obj.list).each(function () {
                            console.log(data);
                            num += 1;
                            str += "<li class=\"scListArea borCdbd7d6 productwrapper border\">" +
                                "<div  class=\"pic\">" +
                                "<a href=\"searchDetails.jsp?id=" + this.id + "\" class=\"track\" title='" + this.name + "' target=\"_blank\">" +
                                "<img class=\"productPhoto\" src='" + this.picture.picture1 + "' alt='" + this.name + " 'width=\"230\" height=\"230\" /></a>" +
                                "<div class=\"vancllist_logo\"></div></div>" +
                                "<p><a href=\"searchDetails.jsp?id=" + this.id + "\" class=\"track\" title='" + this.name + "' target=\"_blank\"></a>" + this.name + "</p>" +
                                "<div class=\"Mpricediv0124\">" +
                                "<span class=\"Sprice\">售价￥" + this.price + "</span></div></li>";
                        })
                    }
                    str2+="找到和<span>"+name+"</span>相关商品<span>"+num+"</span>款";
                    str += "</ul>";
                    $("#vanclproducts").append(str);
                    $(".tsmsg").append(str2);

                    var page = "";
                    page += "<nav aria-label=\"Page navigation\">" +
                        "<ul class=\"pagination\">" +
                        "<li><a href=\"javascript:void(0)\" onclick='query(1,"+ps+")'>首页</a></li>";

                    if(obj.hasPreviousPage){
                        page += "<li>" +
                            "<a href=\"javascript:void(0)\" aria-label=\"Previous\" onclick=\"query("+(obj.pageNum-1)+","+ps+")\">" +
                            "<span aria-hidden=\"true\">&laquo;</span>" +
                            "</a>" +
                            "</li>";
                    }

                    $(obj.navigatepageNums).each(function () {
                        if(obj.pageNum==this){
                            page += "<li class='active'><a href=\"javascript:void(0)\" onclick='query("+this+","+ps+")'>"+this+"</a></li>";
                        }else{
                            page += "<li><a href=\"javascript:void(0)\" onclick='query("+this+","+ps+")'>"+this+"</a></li>";
                        }

                    })

                    if(obj.hasNextPage){
                        page += "<li>\n" +
                            "<a href=\"javascript:void(0)\" aria-label=\"Next\" onclick='query("+(obj.pageNum+1)+","+ps+")'>" +
                            "<span aria-hidden=\"true\">&raquo;</span>" +
                            "</a>" +
                            "</li>";
                    }
                    page += "<li><a href=\"javascript:void(0)\" onclick='query("+obj.pages+","+ps+")'>未页</a></li>" +
                        "</ul>" +
                        "</nav>";
                    $("nav").empty();
                    $("nav").append(page);
                });
            })


            //销量按钮点击时
            $(".xiaoliang a").click(function () {
                $("#vanclproducts").empty();
                $(".tsmsg").empty();
                $("nav").empty();
                $.getJSON("games/show2",{"pn":pn, "ps":ps,"gameName":name},function (data) {
                    var obj = eval(data);
                    var str = "<ul>";
                    var str2="";
                    var num=0;
                    if(data=="" || data==null){
                        str+="<p>没有找到<span style=\"font-weight:bolder;\">"+name+"</span>相关的内容</p>"
                        str2+="找到和<span>"+name+"</span>相关商品<span>"+num+"</span>款";
                        $("#vanclproducts").append(str);
                        $(".tsmsg").append(str2);
                    }else {
                        $(obj.list).each(function () {
                            console.log(data);
                            num += 1;
                            str += "<li class=\"scListArea borCdbd7d6 productwrapper border\">" +
                                "<div  class=\"pic\">" +
                                "<a href=\"searchDetails.jsp?id=" + this.id + "\" class=\"track\" title='" + this.name + "' target=\"_blank\">" +
                                "<img class=\"productPhoto\" src='" + this.picture.picture1 + "' alt='" + this.name + " 'width=\"230\" height=\"230\" /></a>" +
                                "<div class=\"vancllist_logo\"></div></div>" +
                                "<p><a href=\"searchDetails.jsp?id=" + this.id + "\" class=\"track\" title='" + this.name + "' target=\"_blank\"></a>" + this.name + "</p>" +
                                "<div class=\"Mpricediv0124\">" +
                                "<span class=\"Sprice\">售价￥" + this.price + "</span></div></li>";
                        })
                    }
                    str2+="找到和<span>"+name+"</span>相关商品<span>"+num+"</span>款";
                    str += "</ul>";
                    $("#vanclproducts").append(str);
                    $(".tsmsg").append(str2);

                    var page = "";
                    page += "<nav aria-label=\"Page navigation\">" +
                        "<ul class=\"pagination\">" +
                        "<li><a href=\"javascript:void(0)\" onclick='query(1,"+ps+")'>首页</a></li>";

                    if(obj.hasPreviousPage){
                        page += "<li>" +
                            "<a href=\"javascript:void(0)\" aria-label=\"Previous\" onclick=\"query("+(obj.pageNum-1)+","+ps+")\">" +
                            "<span aria-hidden=\"true\">&laquo;</span>" +
                            "</a>" +
                            "</li>";
                    }

                    $(obj.navigatepageNums).each(function () {
                        if(obj.pageNum==this){
                            page += "<li class='active'><a href=\"javascript:void(0)\" onclick='query("+this+","+ps+")'>"+this+"</a></li>";
                        }else{
                            page += "<li><a href=\"javascript:void(0)\" onclick='query("+this+","+ps+")'>"+this+"</a></li>";
                        }

                    })

                    if(obj.hasNextPage){
                        page += "<li>\n" +
                            "<a href=\"javascript:void(0)\" aria-label=\"Next\" onclick='query("+(obj.pageNum+1)+","+ps+")'>" +
                            "<span aria-hidden=\"true\">&raquo;</span>" +
                            "</a>" +
                            "</li>";
                    }
                    page += "<li><a href=\"javascript:void(0)\" onclick='query("+obj.pages+","+ps+")'>未页</a></li>" +
                        "</ul>" +
                        "</nav>";
                    $("nav").empty();
                    $("nav").append(page);
                });
            })


            //好评点击
            $(".haoping a").click(function () {
                $("#vanclproducts").empty();
                $(".tsmsg").empty();
                $("nav").empty();
                $.getJSON("games/show3",{"pn":pn, "ps":ps,"gameName":name},function (data) {
                    var obj = eval(data);
                    var str = "<ul>";
                    var str2="";
                    var num=0;
                    if(data=="" || data==null){
                        str+="<p>没有找到<span style=\"font-weight:bolder;\">"+name+"</span>相关的内容</p>"
                        str2+="找到和<span>"+name+"</span>相关商品<span>"+num+"</span>款";
                        $("#vanclproducts").append(str);
                        $(".tsmsg").append(str2);
                    }else {
                        $(obj.list).each(function () {
                            console.log(data);
                            num += 1;
                            str += "<li class=\"scListArea borCdbd7d6 productwrapper border\">" +
                                "<div  class=\"pic\">" +
                                "<a href=\"searchDetails.jsp?id=" + this.id + "\" class=\"track\" title='" + this.name + "' target=\"_blank\">" +
                                "<img class=\"productPhoto\" src='" + this.picture.picture1 + "' alt='" + this.name + " 'width=\"230\" height=\"230\" /></a>" +
                                "<div class=\"vancllist_logo\"></div></div>" +
                                "<p><a href=\"searchDetails.jsp?id=" + this.id + "\" class=\"track\" title='" + this.name + "' target=\"_blank\"></a>" + this.name + "</p>" +
                                "<div class=\"Mpricediv0124\">" +
                                "<span class=\"Sprice\">售价￥" + this.price + "</span></div></li>";
                        })
                    }
                    str2+="找到和<span>"+name+"</span>相关商品<span>"+num+"</span>款";
                    str += "</ul>";
                    $("#vanclproducts").append(str);
                    $(".tsmsg").append(str2);

                    var page = "";
                    page += "<nav aria-label=\"Page navigation\">" +
                        "<ul class=\"pagination\">" +
                        "<li><a href=\"javascript:void(0)\" onclick='query(1,"+ps+")'>首页</a></li>";

                    if(obj.hasPreviousPage){
                        page += "<li>" +
                            "<a href=\"javascript:void(0)\" aria-label=\"Previous\" onclick=\"query("+(obj.pageNum-1)+","+ps+")\">" +
                            "<span aria-hidden=\"true\">&laquo;</span>" +
                            "</a>" +
                            "</li>";
                    }

                    $(obj.navigatepageNums).each(function () {
                        if(obj.pageNum==this){
                            page += "<li class='active'><a href=\"javascript:void(0)\" onclick='query("+this+","+ps+")'>"+this+"</a></li>";
                        }else{
                            page += "<li><a href=\"javascript:void(0)\" onclick='query("+this+","+ps+")'>"+this+"</a></li>";
                        }

                    })

                    if(obj.hasNextPage){
                        page += "<li>\n" +
                            "<a href=\"javascript:void(0)\" aria-label=\"Next\" onclick='query("+(obj.pageNum+1)+","+ps+")'>" +
                            "<span aria-hidden=\"true\">&raquo;</span>" +
                            "</a>" +
                            "</li>";
                    }
                    page += "<li><a href=\"javascript:void(0)\" onclick='query("+obj.pages+","+ps+")'>未页</a></li>" +
                        "</ul>" +
                        "</nav>";
                    $("nav").empty();
                    $("nav").append(page);
                });
            })


            //价格升序按钮点击时
            $(".jiageHover").click(function () {
                $("#vanclproducts").empty();
                $(".tsmsg").empty();
                $("nav").empty();
                $.getJSON("games/show5",{"pn":pn, "ps":ps,"gameName":name},function (data) {
                    var obj = eval(data);
                    var str = "<ul>";
                    var str2="";
                    var num=0;
                    if(data=="" || data==null){
                        str+="<p>没有找到<span style=\"font-weight:bolder;\">"+name+"</span>相关的内容</p>"
                        str2+="找到和<span>"+name+"</span>相关商品<span>"+num+"</span>款";
                        $("#vanclproducts").append(str);
                        $(".tsmsg").append(str2);
                    }else {
                        $(obj.list).each(function () {
                            console.log(data);
                            num += 1;
                            str += "<li class=\"scListArea borCdbd7d6 productwrapper border\">" +
                                "<div  class=\"pic\">" +
                                "<a href=\"searchDetails.jsp?id=" + this.id + "\" class=\"track\" title='" + this.name + "' target=\"_blank\">" +
                                "<img class=\"productPhoto\" src='" + this.picture.picture1 + "' alt='" + this.name + " 'width=\"230\" height=\"230\" /></a>" +
                                "<div class=\"vancllist_logo\"></div></div>" +
                                "<p><a href=\"searchDetails.jsp?id=" + this.id + "\" class=\"track\" title='" + this.name + "' target=\"_blank\"></a>" + this.name + "</p>" +
                                "<div class=\"Mpricediv0124\">" +
                                "<span class=\"Sprice\">售价￥" + this.price + "</span></div></li>";
                        })
                    }
                    str2+="找到和<span>"+name+"</span>相关商品<span>"+num+"</span>款";
                    str += "</ul>";
                    $("#vanclproducts").append(str);
                    $(".tsmsg").append(str2);

                    var page = "";
                    page += "<nav aria-label=\"Page navigation\">" +
                        "<ul class=\"pagination\">" +
                        "<li><a href=\"javascript:void(0)\" onclick='query(1,"+ps+")'>首页</a></li>";

                    if(obj.hasPreviousPage){
                        page += "<li>" +
                            "<a href=\"javascript:void(0)\" aria-label=\"Previous\" onclick=\"query("+(obj.pageNum-1)+","+ps+")\">" +
                            "<span aria-hidden=\"true\">&laquo;</span>" +
                            "</a>" +
                            "</li>";
                    }

                    $(obj.navigatepageNums).each(function () {
                        if(obj.pageNum==this){
                            page += "<li class='active'><a href=\"javascript:void(0)\" onclick='query("+this+","+ps+")'>"+this+"</a></li>";
                        }else{
                            page += "<li><a href=\"javascript:void(0)\" onclick='query("+this+","+ps+")'>"+this+"</a></li>";
                        }

                    })

                    if(obj.hasNextPage){
                        page += "<li>\n" +
                            "<a href=\"javascript:void(0)\" aria-label=\"Next\" onclick='query("+(obj.pageNum+1)+","+ps+")'>" +
                            "<span aria-hidden=\"true\">&raquo;</span>" +
                            "</a>" +
                            "</li>";
                    }
                    page += "<li><a href=\"javascript:void(0)\" onclick='query("+obj.pages+","+ps+")'>未页</a></li>" +
                        "</ul>" +
                        "</nav>";
                    $("nav").empty();
                    $("nav").append(page);
                });
            })
            //价格降序按钮点击时
            $(".jiageDown a").click(function () {
                $("#vanclproducts").empty();
                $(".tsmsg").empty();
                $("nav").empty();
                $.getJSON("games/show6",{"pn":pn, "ps":ps,"gameName":name},function (data) {
                    var obj = eval(data);
                    var str = "<ul>";
                    var str2="";
                    var num=0;
                    if(data=="" || data==null){
                        str+="<p>没有找到<span style=\"font-weight:bolder;\">"+name+"</span>相关的内容</p>"
                        str2+="找到和<span>"+name+"</span>相关商品<span>"+num+"</span>款";
                        $("#vanclproducts").append(str);
                        $(".tsmsg").append(str2);
                    }else {
                        $(obj.list).each(function () {
                            console.log(data);
                            num += 1;
                            str += "<li class=\"scListArea borCdbd7d6 productwrapper border\">" +
                                "<div  class=\"pic\">" +
                                "<a href=\"searchDetails.jsp?id=" + this.id + "\" class=\"track\" title='" + this.name + "' target=\"_blank\">" +
                                "<img class=\"productPhoto\" src='" + this.picture.picture1 + "' alt='" + this.name + " 'width=\"230\" height=\"230\" /></a>" +
                                "<div class=\"vancllist_logo\"></div></div>" +
                                "<p><a href=\"searchDetails.jsp?id=" + this.id + "\" class=\"track\" title='" + this.name + "' target=\"_blank\"></a>" + this.name + "</p>" +
                                "<div class=\"Mpricediv0124\">" +
                                "<span class=\"Sprice\">售价￥" + this.price + "</span></div></li>";
                        })
                    }
                    str2+="找到和<span>"+name+"</span>相关商品<span>"+num+"</span>款";
                    str += "</ul>";
                    $("#vanclproducts").append(str);
                    $(".tsmsg").append(str2);

                    var page = "";
                    page += "<nav aria-label=\"Page navigation\">" +
                        "<ul class=\"pagination\">" +
                        "<li><a href=\"javascript:void(0)\" onclick='query(1,"+ps+")'>首页</a></li>";

                    if(obj.hasPreviousPage){
                        page += "<li>" +
                            "<a href=\"javascript:void(0)\" aria-label=\"Previous\" onclick=\"query("+(obj.pageNum-1)+","+ps+")\">" +
                            "<span aria-hidden=\"true\">&laquo;</span>" +
                            "</a>" +
                            "</li>";
                    }

                    $(obj.navigatepageNums).each(function () {
                        if(obj.pageNum==this){
                            page += "<li class='active'><a href=\"javascript:void(0)\" onclick='query("+this+","+ps+")'>"+this+"</a></li>";
                        }else{
                            page += "<li><a href=\"javascript:void(0)\" onclick='query("+this+","+ps+")'>"+this+"</a></li>";
                        }

                    })

                    if(obj.hasNextPage){
                        page += "<li>\n" +
                            "<a href=\"javascript:void(0)\" aria-label=\"Next\" onclick='query("+(obj.pageNum+1)+","+ps+")'>" +
                            "<span aria-hidden=\"true\">&raquo;</span>" +
                            "</a>" +
                            "</li>";
                    }
                    page += "<li><a href=\"javascript:void(0)\" onclick='query("+obj.pages+","+ps+")'>未页</a></li>" +
                        "</ul>" +
                        "</nav>";
                    $("nav").empty();
                    $("nav").append(page);
                });
            })
        }

    </script>

</head>
<body id="body" class="cols_manage full">
<!-- 修改full判断 之前full是指1200 现在的full是指980 由首页统一修改 -->
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="images"></div>
<%@include file="pages/publicPages/head.jsp"%>
<span class="blank10"></span>


<div style="margin: 0 auto;" class="">
    <span class="selectareali"></span>
    <div class="sr_contation">
        <div class="sr_left" id="vjiaTop">
            <div class="breadnav">
                <div class="locationdiv">
                    <span>当前位置：</span>
                </div>
                <div class="tsmsg">

                </div>
            </div>
            <div class="selectarea">
            </div>
            <div id="recommendkeywords" style="display: none; height: 0px; width: 100%; overflow: hidden;">
            </div>
            <div id="floatposition" style="margin-bottom: 15px">
            </div>

            <div id="floatdiv" style="background-color: White; z-index: 80;">
                <div class="filterTabBarN5">
                    <ul class="searchTabbarN5">
                        <li class="hover" ><a class="track" name="s-s-link-all" href="#">全部商品</a></li>
                    </ul>
                    <div class="pageBoxN5">

                    </div>
                </div>
                <div class="filterFormN5">
                    <div class="filterForm0703">
                        <div class="searchCol">
                            <ul>
                                <li class="moren">
                                    <a title="按推荐由高到低" name="s_order_0_10" class="track" >
                                        <em>默认</em>
                                        <span class="upTrendBottom">按推荐由高到低</span></a></li>
                                <li class="xiaoliang">
                                    <a title="按销量由高到低" name="s_order_0_2" class="track" >
                                        <em>销量</em>
                                        <span class="upTrendBottom">按销量由高到低</span></a></li>

                                <li class="haoping">
                                    <a title="按评价从高到低" name="s_order_0_5" class="track" >
                                        <em>好评</em>
                                        <span class="upTrendBottom">按评价从高到低</span></a></li>
                                <li class="jiage jiageHover" id="sortTypeMore" >
                                    <a title="按价格从低到高" name="s_order_0_3" class="track" >
                                        <em>价格</em>
                                        <span class="BottomTrendUpRed">按价格从低到高</span></a></li>
                                <li class="jiage jiageDown">
                                    <a title="按价格从高到低" name="s_order_0_4" class="track" >
                                        <em>价格</em>
                                        <span class="upTrendBottom">按价格从高到低</span></a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


            <span class="blank15"></span>

            <div class="pruarea pruarea0124" id="vanclproducts" counts="30">
            </div>

            <span class="blank0"></span>
            <span class="blank25"></span>

            <div class="row text-center fr">
                <nav aria-label="Page navigation">
                </nav>
            </div>
        </div>
    </div>
</div>
    <div class="blank0">
    </div>
</div>
<%@include file="pages/publicPages/footer.jsp"%>

</div>
</body>
</html>
