package com.kgc.mapper;

import com.kgc.entity.Collection;
import com.kgc.entity.Game;
import com.kgc.entity.GameNote;
import com.kgc.entity.GameS;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GameKindMapper {

    List<GameS> queryByType(@Param("type") String type);
    // 按照销量排序
    List<GameS> queryByTypeAndSale(@Param("type") String type);
    // 按照好评排序
    List<GameS> queryByTypeAndPl(@Param("type") String type);
    // 按照价格排序升序
    List<GameS> queryByTypeAndPriceUp(@Param("type") String type);
    // 按照价格排排序降序
    List<GameS> queryByTypeAndPriceDown(@Param("type") String type);
}
