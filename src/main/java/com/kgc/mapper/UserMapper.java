package com.kgc.mapper;


import com.kgc.entity.Collection;
import com.kgc.entity.Order;
import com.kgc.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    //  登录时验证用户名密码是否正确
    boolean queryByPhoneOrEmailAndPwd(String loginName, String password);
    // 查询所有用户
    public List<User> queryAllUser();
    //根据用户id删除用户
    public int delUser(@Param("id") int id);
    //根据用户id查询购物车
    List<Collection> queryShoppingCart(@Param("id") int id);
    //根据关系id删除购物车商品
    int delShoppingInCart(@Param("id") int id);
    //增加收藏
    int addCollection(@Param("uid")String uid,@Param("sid")String sid);
    //根据关系id删除收藏
    int delCollection(@Param("id")int id);
    //根据用户id和订单状态查询该用户的订单
    List<Order> querryOrderListByIdAndType(@Param("id")int id,@Param("type")int type);
    //用户收货
    int confirmOrder(@Param("id") int id);
    //
    User queryUser(@Param("name")String name,@Param("pwd")String pwd);

    //用户注册，增加用户信息
    public  int addUser(@Param("user") User user);
    //检查手机号是否已注册
    int checkPhone(@Param("phoneNo") String phoneNo);
    //根据id查询用户
    User queryUserById(@Param("id") int id);
    //更改购物车
    int updateShoppingCart(@Param("id")int id,@Param("sid")int sid,@Param("count")int count);
    //根据用户id和商品id删除购物车
    int delCart(@Param("uid")int uid,@Param("sid")int sid);
    //更改用户昵称
    int updateUserId(@Param("id") int id,@Param("nickName") String nickName);
    //增加购物车
    int addShoppingCart(@Param("uid")int uid,@Param("sid")int sid,@Param("count")int count);

    int updatePwd(@Param("pwd")String pwd,@Param("phone")String phone);
 }
