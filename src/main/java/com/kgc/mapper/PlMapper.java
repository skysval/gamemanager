package com.kgc.mapper;

import com.kgc.entity.Evaluate;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PlMapper {

    // 查找全部评论；
    List<Evaluate> queryListPl(@Param("id") int g_id);


    // 添加评论
    int addPl(@Param("evaluate") Evaluate evaluate);
    //删除评论
    int delPl(@Param("id") int id);

}
