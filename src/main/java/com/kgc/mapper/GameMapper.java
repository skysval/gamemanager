package com.kgc.mapper;

import com.kgc.entity.Collection;
import com.kgc.entity.Game;
import com.kgc.entity.GameNote;
import com.kgc.entity.GameS;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GameMapper {
    // 获取游戏列表
    List<Game> queryGameList();
    // 根据游戏名查找游戏；
    List<Game> queryGameByName(String gameName);
    //表关联查询所有游戏
    public List<Game> queryGame();
    //游戏表查询游戏
    public List<Game> queryAllGame();
    //根据用户id查询收藏
    List<Collection> queryCollection(@Param("id") int id);
    //根据id查找游戏
    List<GameNote> queryGameById(Integer[] ids);
    //根据游戏id删除游戏
    public int delGame(@Param("id") int id);
    //根据id删除图片
    public int delPicture(@Param("id") int id);
    //根据id修改游戏价格
    public int updatePrice(@Param("id")int id,@Param("price")double price);
    //查找游戲数量
    public int queryCount(@Param("gid")int gid,@Param("uid")int uid);
    //关系表
    public int addRelation(@Param("order")String order,@Param("shopid")int shopid,@Param("count")int count);




    GameS queryById(@Param("id") int id);
    // 根据游戏名查找游戏；默序认排
    List<GameS> queryByName(@Param("name") String gameName);
    // 按照销量排序
    List<GameS> queryByNameAndSale(@Param("name") String gameName);
    // 按照好评排序
    List<GameS> queryByNameAndPl(@Param("name") String gameName);
    // 按照价格排序升序
    List<GameS> queryByNameAndPriceUp(@Param("name") String gameName);
    // 按照价格排排序降序
    List<GameS> queryByNameAndPriceDown(@Param("name") String gameName);
}
