package com.kgc.mapper;

import com.kgc.entity.Address;
import org.apache.ibatis.annotations.Param;
import java.util.List;

public interface AddressMapper {
    //查询地址
    public List<Address> queryAddress(@Param("userId") int userId);
    //删除地址
    public int delAddress(@Param("id")int id );
    //增加地址
    public int addAddress(@Param("address") Address address);
    //根据id查找地址
    public Address queryAddressById(@Param("id") int id);
    //设置默认地址
    public int removeMr(@Param("userid") int userid);
    public int addMr(@Param("addressid")int addressid);
}
