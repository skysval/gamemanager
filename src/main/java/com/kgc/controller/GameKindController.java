package com.kgc.controller;

import com.kgc.entity.GameS;
import com.kgc.service.GameKindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("type")
@ResponseBody
public class GameKindController {
    @Autowired
    private GameKindService gameKindService;
    // 通过种类查询进行展示
    @RequestMapping("queryByType")
    public List<GameS> queryByType(String type){
        List<GameS> games = gameKindService.queryByType(type);
        return games;
    }


    //页面展示销量排序，
    @RequestMapping("show2")
    public List<GameS> queryByTypeAndSale(String type){
        List<GameS> games = gameKindService.queryByTypeAndSale(type);
        return  games;
    }
    //按照好评排序
    @RequestMapping("show3")
    public List<GameS> queryByTypeAndPl(String type){
        List<GameS> games = gameKindService.queryByTypeAndPl(type);
        return  games;
    }


    // 按照价格排序升序
    @RequestMapping("show5")
    public List<GameS> queryByTypeAndPriceUp(String type){
        List<GameS> games = gameKindService.queryByTypeAndPriceUp(type);
        return  games;
    }

    // 按照价格排序降序
    @RequestMapping("show6")
    public List<GameS> queryByTypeAndPriceDowm(String type){
        List<GameS> games = gameKindService.queryByTypeAndPriceDown(type);
        return  games;
    }
}
