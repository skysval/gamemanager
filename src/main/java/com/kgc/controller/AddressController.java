package com.kgc.controller;

import com.kgc.entity.Address;
import com.kgc.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.UnsupportedEncodingException;
import java.util.List;

@Controller
@RequestMapping("address")
@ResponseBody
public class AddressController {

    @Autowired
    private AddressService addressService;


    @RequestMapping("queryAll")
    public List<Address> queryAll(int userId){
        return addressService.queryAddress(userId);
    }

    @RequestMapping("del")
    public boolean delAddress(int id){
        return addressService.delAddress(id);
    }

    @RequestMapping("add")
    public boolean addAddress(Address address) throws UnsupportedEncodingException {
        return addressService.addAddress(address);
    }


    @RequestMapping("queryAddressById")
    public Address queryAddressById(int id){
        return addressService.queryAddressById(id);
    }

    @RequestMapping("mrid")
    public boolean updateMr(int userid,int addressid){
        addressService.removeMr(userid);
        addressService.addMr(addressid);
        return addressService.addMr(addressid)==1?true:false;
    }

}
