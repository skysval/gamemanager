package com.kgc.controller;

import com.kgc.entity.Evaluate;
import com.kgc.service.PlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
@RequestMapping("pl")
@ResponseBody
public class PlController {
    @Autowired
    private PlService plService;

    @RequestMapping("query")
    public List<Evaluate> query(@RequestParam("id") int id){
        List<Evaluate> plList = plService.queryListPl(id);
        return  plList;
    }

    @RequestMapping("add")
    public boolean add(int id , int u_id , String context,int plnum){
        /*// 将评论数量传值进去；
        GameS game  = new GameS();
        game.setPl(plnum);
        game.setId(id);
        plService.update(game);*/
        Evaluate evaluate = new Evaluate();
        evaluate.setId(id);
        evaluate.setU_id(u_id);
        evaluate.setContext(context);
        String time= LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        evaluate.setTime(time);
        boolean flag = plService.addPl(evaluate);
        if(flag){
            return  true;
        }
        return false;
    }

    @RequestMapping("del")
    public void del(int id , int u_id , String context) {
        boolean delFlag = plService.delPl(id);
    }
}
