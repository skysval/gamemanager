package com.kgc.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kgc.entity.Order;
import com.kgc.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RequestMapping("order")
@Controller
@ResponseBody
public class OrderController {

    @Autowired
    private OrderService orderService;


    @RequestMapping("queryAll")
    public PageInfo queryAll(@RequestParam(value = "pn", defaultValue = "1") Integer pn,
                             @RequestParam(value = "ps", defaultValue = "10") Integer ps){
        PageHelper.startPage(pn,ps);
        List<Order> ordersList = orderService.queryOrder();
        PageInfo info = new PageInfo(ordersList,5);
        return info;
    }

    @RequestMapping("addOrder")
    public String[] addOrder(Order order, HttpServletRequest request) throws UnsupportedEncodingException {
        // 创建唯一订单号
        int random = (int) (Math.random() * 10000);
        String dateStr = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        // 订单号拼接规则：手机号后四位+当前时间后四位+随机数四位数
        String out_trade_no = order.getPhone().substring(7) + dateStr.substring(10)
                + random;
        order.setNum(out_trade_no);
        orderService.addOrder(order);
        request.getSession().setAttribute("shopId",order.getId());
        return new String[]{out_trade_no};
    }
    @RequestMapping("deliver")
    public boolean deliver(int id){
        return orderService.deliver(id);
    }
}
