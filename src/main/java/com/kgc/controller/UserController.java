package com.kgc.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kgc.entity.Collection;
import com.kgc.entity.Order;
import com.kgc.entity.User;
import com.kgc.service.UserService;
import com.kgc.utils.Random6;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("user")
@ResponseBody
public class UserController {
    private int code;
    private String phoneNo;

    @Autowired
    public UserService userService;

    @RequestMapping("query1")
    public boolean queryByPhoneOrEmailAndPwd(@Param("userName") String loginName, @Param("password")String password){
    return  false;
    }
    @RequestMapping("queryAllUser")
    public List<User> queryAll(){
        return userService.queryAllUser();
    }
    @RequestMapping("delUser")
    public boolean delUser(int id){
        return userService.delUser(id);
    }
    @RequestMapping("queryShoppingCart")
    public List<Collection> queryShoppingCart(int id) {
        return userService.queryShoppingCart(id);
    }
    @RequestMapping("delCart")
    public int[] delShoppingInCart(@RequestParam("id") int id) {
        return new int[]{userService.delShoppingInCart(id)};
    }
    @RequestMapping("addCol")
    public int[] addCollection(String uid, String sid) {
        return new int[]{userService.addCollection(uid, sid)};
    }
    @RequestMapping("delCol")
    public int[] delCollection(@RequestParam("id") int id) {
        return new int[] {userService.delCollection(id)};
    }

    @RequestMapping("queryOrder")
    public PageInfo querryOrderListByIdAndType(int id,int type,@RequestParam(value = "pn",defaultValue="1")Integer pn) {
        PageHelper.startPage(pn,5);
        List<Order> orders = userService.querryOrderListByIdAndType(id,type);
        PageInfo page = new PageInfo(orders,8);
        return page;
    }
    @RequestMapping("confirmOrder")
    int[] confirmOrder(int id){
        return new int[] {userService.confirmOrder(id)};
    };

    @RequestMapping("login")
    public Object[] login(String username, String password, HttpServletRequest req){
        User user = userService.queryUser(username, password);
        if (user != null) {
            HttpSession session = req.getSession();
            session.setAttribute("id", user.getId());
            session.setAttribute("type",user.getUserType());
            return new Object[]{true,user.getUserType()};
        }
        return new Object[]{false};
    }
    @RequestMapping("sendSms")
    public void sendSms(String phoneNo){
        code = (int)(Math.random()*10000);
        this.phoneNo = phoneNo;
        Random6.getCode(code,phoneNo);
    }
    @RequestMapping("resgin")
    public int Resgin(String phoneNo,int code,String pwd,HttpServletRequest request){
        User user = new User();
        user.setPhone(phoneNo);
        user.setPassword(pwd);
        boolean flag = false;
        flag = userService.addUser(user);
        if (phoneNo.equals(this.phoneNo) && this.code == code && flag == true){
            request.getSession().setAttribute("id",user.getId());
            request.getSession().setAttribute("type",1);
            return user.getId();
        }else{
            return 0;
        }

    }
    @RequestMapping("checkPhone")
    public int checkPhone(String phoneNo) {
        return userService.checkPhone(phoneNo);
    }
    @RequestMapping("queryUserById")
    public User queryUserById(int id) {
        return userService.queryUserById(id);
    }

    @RequestMapping("delSession")
    public void delSession(HttpServletRequest request){
        request.getSession().setAttribute("id","");
    }

    @RequestMapping("updateShoppingCart")
    public void updateShoppingCart(int id, int sid, int count) {
        userService.updateShoppingCart(id,sid,count);
    }

    @RequestMapping("delCartByUidAndSid")
    public int delCart(int uid, int sid) {
        return userService.delCart(uid,sid);
    }

    @RequestMapping("updateUserId")
    public int updateUserId(int id, String nickName) {
        return userService.updateUserId(id,nickName);
    }

    @RequestMapping("addShoppingCart")
    public int addShoppingCart(int uid, int sid, int count) {
        return userService.addShoppingCart(uid,sid,count);
    }

    @RequestMapping("forgetpwd")
    public boolean resetpwd(String phone , int code){
        if(phone.equals(this.phoneNo) && this.code == code){
            return  true;
        }
        return false;
    }
    @RequestMapping("updatepwd")
    public boolean changePwd(String pwd , String phone){
        boolean flag = userService.updatePwd(pwd,phone);
        if(flag){
            return  true;
        }
        return false;
    }

    @RequestMapping("check")
    public boolean check(String phone){
        List<User> list = userService.queryAllUser();
        for(User user : list){
            if(user.getPhone().equals(phone)){
                System.out.println(phone+"--"+user.getPhone());
                return true;
            }
        }
        return false;
    }

}
