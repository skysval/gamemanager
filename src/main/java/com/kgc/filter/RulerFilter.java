package com.kgc.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RulerFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest)req;
        HttpServletResponse response = (HttpServletResponse)resp;
        if (request.getSession().getAttribute("type")!=null){
            int id = (int)request.getSession().getAttribute("type");
            System.out.println(id);
            if (id == 2){
                chain.doFilter(req, resp);
                return;
            }else {
                response.sendRedirect("/Game/stone.jsp");
                return;
            }
        }
        response.sendRedirect("/Game/stone.jsp");
        return;
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
