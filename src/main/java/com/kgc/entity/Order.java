package com.kgc.entity;

import java.util.List;

public class Order {
    private int id;
    private String name;
    private String shop;
    private String num;
    private String phone;
    private String address;
    private int status;
    private int userid;
    private double totalprice;
    private List<OrderNote> gameList;

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public double getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(double totalprice) {
        this.totalprice = totalprice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<OrderNote> getGameList() {
        return gameList;
    }

    public void setGameList(List<OrderNote> gameList) {
        this.gameList = gameList;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", shop='" + shop + '\'' +
                ", num='" + num + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", status=" + status +
                ", userid=" + userid +
                ", totalprice=" + totalprice +
                ", gameList=" + gameList +
                '}';
    }
}
