package com.kgc.entity;



/*
       评论实体类
 */
public class Evaluate {
    private int id;
    private int u_id;
    private String context;
    private String time;
    private User user;

    @Override
    public String toString() {
        return "Evaluate{" +
                "id=" + id +
                ", u_id=" + u_id +
                ", context='" + context + '\'' +
                ", time=" + time +
                ", user=" + user +
                '}';
    }

    public int getU_id() {
        return u_id;
    }

    public void setU_id(int u_id) {
        this.u_id = u_id;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
