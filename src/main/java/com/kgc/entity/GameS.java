package com.kgc.entity;

/*
    搜索页面
    游戏类
 */
public class GameS {
    // 游戏id  ，  游戏名 ， 价格 ， 游戏类型 ， 开发公司 ， 游戏支持平台 ， 游戏发布时间 ，
    private Integer id;
    private String name;
    private double price;
    private String type;
    private String publisher;
    private  String developers;
    private String platfrom;
    private String issueDate;
    private String evaluation;
    private String content;
    private int salevolume;
    private int pl;
    private Picture picture;

    public int getSalevolume() {
        return salevolume;
    }

    public void setSalevolume(int salevolume) {
        this.salevolume = salevolume;
    }

    public int getPl() {
        return pl;
    }

    public void setPl(int pl) {
        this.pl = pl;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getDevelopers() {
        return developers;
    }

    public void setDevelopers(String developers) {
        this.developers = developers;
    }

    public String getPlatfrom() {
        return platfrom;
    }

    public void setPlatfrom(String platfrom) {
        this.platfrom = platfrom;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", type='" + type + '\'' +
                ", publisher='" + publisher + '\'' +
                ", developers='" + developers + '\'' +
                ", platfrom='" + platfrom + '\'' +
                ", issueDate='" + issueDate + '\'' +
                ", evaluation='" + evaluation + '\'' +
                ", content='" + content + '\'' +
                ", salevolume=" + salevolume +
                ", pl=" + pl +
                ", picture=" + picture +
                '}';
    }
}
