package com.kgc.entity;

public class GameNote {
    private int id;
    private String name;
    private double price;
    private String picture1;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPicture1() {
        return picture1;
    }

    public void setPicture1(String picture1) {
        this.picture1 = picture1;
    }

    @Override
    public String toString() {
        return "GameNote{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", picture1='" + picture1 + '\'' +
                '}';
    }
}
