package com.kgc.entity;

public class OrderNote {
    private int id;
    private String name;
    private double price;
    private String picture1;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPicture1() {
        return picture1;
    }

    public void setPicture1(String picture1) {
        this.picture1 = picture1;
    }

    @Override
    public String toString() {
        return "OrderNote{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", picture1='" + picture1 + '\'' +
                ", count=" + count +
                '}';
    }
}
