package com.kgc.entity;

public class AlipayAttr {
    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2016101600703117";


    // 商户应用私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCObTWstZob92vOBGhcRoMlZykh8peqwYH2fwEeSxN8ukYr6EJVf5mQ4WzM6FW0CPYD/wOrJW8gxy13HEp9yGRYYPCo/Rryls3EToahjAKiI4BkOfvvSH8NkURbwR90VNeX/B42IeRs92QPVXLrqVzlB2L1+ND4aeY4Mjr3u0EQNdpzhi6Q7XrOhRPA5GbuiIqA7ie8gTfWlqDSUpTyeEBmtyWN3dp7A8+ZQPzWVBc5pAtIY4Mq+d5bWjV0aPT5t3yXoZ9oMKiOCzZyRyYK1jJbE+tXBiwXqyRs/LepgWvlowpE5K9aHGelEI0T380ABBr7b8i6IG3yUvzxIvEeDsvnAgMBAAECggEAAv2IAvhNJAkt2kV9a3KPjll8I5pUVOF7ORGxlFE/N38xufpGakUjlEcnYaX/JqiJgcv4578Bdk64RafyaHGsqa5fXyzxQ+amq1h6uymVZJIaQAB3qa57/GzwYnzLjEW7wRUqDTjo8uC+2ROSNiB4MtbU3RU8TfrrOoLTeiAUXaK7zq59Lm+Lb463Th8CketPAINhbGUqb9xdbQYr06kUS6XH8YL/yyajQDY5HGpb3F/RrHxPPf1CAm7UV29sMPnpUs3wF/bWKeMVcFKLmj1ONwq0V4YM1ZkLmKsibHmEdblEijB4aKoDQdV2DccG51MqMRGUuUOCAWSPgnwIvA1oAQKBgQDAgEqhEgfjWUFSgU7AaNdHNTvGoSG2fMPNF5JA113jtQLnu/fdUsqC7YgXacVOxJ7P5ilJSlAQ2IdmL1OJZzK8mTqB8APmoyFsrMEz+/7kFCXQI+L/Ag7xDcBfKu/Py93CCnUkcKNnwgnNPA/e82vklEOgCYJvf9TE4YfUvDfxoQKBgQC9aGMFIzoUpfsJR1cCoOC4KoJkIBnE4X0BnFCK1bmgR6Swjl/K5Yn4DDE1TiUOpXO8EjsFW1xoW/g/OHn0bamIC++BlKcm/geuWOABijT9GJ6Xzviw3u9TzB99xsQKYaKg4y2SJI6YeTfi9h9uWxfuLGyR1ciMa1FmYM6a4LJghwKBgQCgUrjVCSVUreN/iX5DNY4GGknclhpz19ljGHyQUE479WekBKLcy7Up8hZkGY1gmtUBpVlBVwjz3sJ10niqeS6CcMOPsIwvLDIMRpzqZLfW0QjlHt5RMC+th5xuJl5wHARnNTp2zPDq22o6agl10BpN10WzJfhiUH6uMn9kKBYXwQKBgQCIiVZQHsvAlMH5N1cOywATYnlqSgWpek7NuWwKGPVeCCc9KvT84yjpCYjzfyiRfU2HtrCbNPfRNvMmR8UKIX9DVi9J/unb3uWUlfasxX9bL8MrdgSqI5rskknlNkKdpTJBvmJcZkPwv/k8f+W6fKoshBOjsTOHndko7wIgfXe4wwKBgAO2oPExxYYGA5w4Rcpv0JAp1Ywpt39fXPdAzcTi2/sMZ4xsZM6cl4vr8oNAuyiGBKI4bZ2GzwuAd5dCYi8IHE4QE/ikmUyKcTNjFNd8Gw3rkgFPas2/vf4Jhj07UjcAbl62iPomhlNdMWc7ONLLJO9OwRtxeWWTbloHRO5TT9FZ";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm
    // 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmgKzDcCU5wexDdwUUBaReCtPc4HzyI7qbmA9ly4DS5UoAf+o0arIRgrrjAXW4ec/J/0UCSLeotmtmWF9KjjQOfYF3teWp/DDHaX+g+/jFe3RX+kxx2Icj8hMNazqsPmq1dx4FULOyfMxuIeHzjAAZyVWob36JbMF3bLiJINj4yMTKnrJM+KxxECzMkEXtVxOpadMVESZvA4Pc+oURHMxku/ZRd4bSAP3FDtAmFAYvPy6WS7mi/2K2lm5afaSEf2gc6Bj343PyHbE+1I26U/f35OWjz9FMgvSA77W8u4FVFLaI0Vt7z/jVVS1eQeSB3A4A9bGTJq3Ic7iPjMDN97q3wIDAQAB";

    // 服务器异步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://localhost:8080/Game/notify_url.do";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://localhost:8080/Game/return/alipayReturn";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";
}
