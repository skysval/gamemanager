package com.kgc.service;
import com.kgc.entity.GameS;

import java.util.List;

public interface GameKindService {



    // 根据游戏种类查找游戏详细信息展示
    List<GameS> queryByType(String type);
    // 按照销量排序
    List<GameS> queryByTypeAndSale(String type);
    // 按照好评排序
    List<GameS> queryByTypeAndPl(String type);

    // 按照价格排序升序
    List<GameS> queryByTypeAndPriceUp(String type);
    // 按照价格排排序降序
    List<GameS> queryByTypeAndPriceDown(String type);
}
