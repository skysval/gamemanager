package com.kgc.service;

import com.kgc.entity.Game;
import com.kgc.entity.addGamePictures;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

public interface RulerService {
    //插入标题图片
    int addFirst(addGamePictures add);
    //插入详情图片
    int addDetail(addGamePictures add);
    //插入游戏信息
    int addGame(Game game);
}
