package com.kgc.service;

import com.kgc.entity.Collection;
import com.kgc.entity.Game;
import com.kgc.entity.GameNote;
import com.kgc.entity.GameS;


import java.util.List;

public interface GameService {
    //表关联查找所有游戏
    public List<Game> queryGame();
    //游戏表
    public List<Game> queryAllGame();
    //查找收藏
    List<Collection> queryCollection(int id);
    //根据id删除游戏
    public boolean delGame(int id);
    //根据id删除照片
    public boolean delPicture(int id);
    //修改价格
    public boolean updatePrice(int id,double price);
    //根据id查找游戏
    List<GameNote> queryGameById(Integer [] id);
    //查找购买游戏数量
    public  int queryCount(int gid,int uid);
    //关系表
    public boolean addRelation(String order,int shopid,int count);
    // 根据id查找游戏详细信息展示
    GameS queryById(int id);
    // 默认按照名字查找
    List<GameS> queryByName(String gameName);
    // 按照销量排序
    List<GameS> queryByNameAndSale(String gameName);
    // 按照好评排序
    List<GameS> queryByNameAndPl(String gameName);
    // 按照价格排序升序
    List<GameS> queryByNameAndPriceUp(String gameName);
    // 按照价格排排序降序
    List<GameS> queryByNameAndPriceDown(String gameName);
}
