package com.kgc.service;


import com.kgc.entity.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderService {
    public List<Order> queryOrder();
    //添加订单
    public boolean addOrder(Order order);
    //发货
    public boolean deliver(int id);

    int getMon(int id);

}
