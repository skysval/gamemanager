package com.kgc.service;

import com.kgc.entity.Address;
import java.util.List;

public interface AddressService {
    //查询用户地址
    public List<Address> queryAddress(int userId);
    //删除地址
    public boolean delAddress(int id);
    //增加地址
    public boolean addAddress(Address address);
    //根据id查地址
    public Address queryAddressById(int id);
    //修改默认
    public int removeMr(int userid);
    public int addMr(int addressid);
}
