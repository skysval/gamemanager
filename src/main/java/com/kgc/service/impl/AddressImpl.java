package com.kgc.service.impl;

import com.kgc.entity.Address;
import com.kgc.mapper.AddressMapper;
import com.kgc.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
@Service
public class AddressImpl implements AddressService {
    @Autowired
    private AddressMapper addressMapper;
    @Override
    public List<Address> queryAddress(int userId) {
        return addressMapper.queryAddress(userId);
    }

    @Override
    public boolean delAddress(int id) {
        return addressMapper.delAddress(id)==1?true:false;
    }

    @Override
    public boolean addAddress(Address address) {
        return addressMapper.addAddress(address)==1?true:false;
    }

    @Override
    public Address queryAddressById(int id) {
        return addressMapper.queryAddressById(id);
    }

    @Override
    public int removeMr(int userid) {
        return addressMapper.removeMr(userid);
    }

    @Override
    public int addMr(int addressid) {
        return addressMapper.addMr(addressid);
    }
}
