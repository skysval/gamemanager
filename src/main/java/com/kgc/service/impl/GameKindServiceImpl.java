package com.kgc.service.impl;

import com.kgc.entity.Collection;
import com.kgc.entity.Game;
import com.kgc.entity.GameNote;
import com.kgc.entity.GameS;
import com.kgc.mapper.GameKindMapper;
import com.kgc.mapper.GameMapper;
import com.kgc.service.GameKindService;
import com.kgc.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GameKindServiceImpl implements GameKindService {
    @Autowired
    private GameKindMapper gameKindMapper;
    @Override
    public List<GameS> queryByType(String type) {
        return gameKindMapper.queryByType(type);
    }

    @Override
    public List<GameS> queryByTypeAndSale(String type) {
        return gameKindMapper.queryByTypeAndSale(type);
    }

    @Override
    public List<GameS> queryByTypeAndPl(String type) {
        return gameKindMapper.queryByTypeAndPl(type);
    }

    @Override
    public List<GameS> queryByTypeAndPriceUp(String type) {
        return gameKindMapper.queryByTypeAndPriceUp(type);
    }

    @Override
    public List<GameS> queryByTypeAndPriceDown(String type) {
        return gameKindMapper.queryByTypeAndPriceDown(type);
    }
}
