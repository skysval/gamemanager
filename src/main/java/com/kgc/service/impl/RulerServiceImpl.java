package com.kgc.service.impl;

import com.kgc.entity.Game;
import com.kgc.entity.addGamePictures;
import com.kgc.mapper.RulerMapper;
import com.kgc.service.RulerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RulerServiceImpl implements RulerService {
    @Autowired
    private RulerMapper rulerMapper;

    @Override
    public int addFirst(addGamePictures add) {
        return rulerMapper.addFirst(add);
    }

    @Override
    public int addDetail(addGamePictures add) {
        return rulerMapper.addDetail(add);
    }

    @Override
    public int addGame(Game game) {
        return rulerMapper.addGame(game);
    }
}
