package com.kgc.service;

import com.kgc.entity.Evaluate;

import java.util.List;

public interface PlService {

    // 查找全部评论；
    List<Evaluate> queryListPl(int id);

    // 添加评论
    boolean addPl(Evaluate evaluate);
    //删除评论
    boolean delPl(int id);

}
